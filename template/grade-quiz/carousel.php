<!-- 
name: carousel grade quiz
author: Paul 
function: Set up the carousel in grade quiz page.
 
modification log:
23:44 6/12/2014 working on the textarea style.
14:56 9/28/2014 grade point follows collect student's answer.
21:31 10/3/2014 dont need to click save grade button now.
17:00 10/5/2014 update quiz grade correspondingly.
-->
<head>
<?php 
  include('css/grade-quiz/carousel-css.php'); 
  include('functions/grade-quiz/postback.php');
  include('functions/get-preview-size.php');
  include('css/grade-quiz/grade-bar-css.php');
  include('js/grade-quiz/grade-quiz-js.php');
  include('js/grade-quiz/tutorial.php');
?>
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<?php
  $user_id = $user['user_id'];
  $q2 = "SELECT * FROM quiz WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
  $r2 = mysqli_query($dbc, $q2);
  $quizInfo = mysqli_fetch_row($r2);
?>
<h1 style="display:inline">Grade Quiz: <?php echo $quizInfo[2]; ?></h1>
<label style="display:hidden;float:right" id="label2" data-placement="right" data-content="Click it again to turn off tutorials." for="tutorial2"></label>
<button type="button" class="btn btn-success" style="float:right" id="tutorial2">Tutorials</button>
</br>

<div class="row">
  <form name="selection-bar">
    
      <div class="col-md-2">
        
      </div>
      <div class="col-md-3">
        <select class="form-control" name="rotate_by" id="rotate-bar" onchange="selectBar()"> 
          <option>rotate by...</option> 
          <?php 
            $user_id = $user['user_id'];
            $q2 = "SELECT td2 FROM user WHERE user_id = '$user_id'";
            $r2 = mysqli_query($dbc, $q2);
            $index = mysqli_fetch_row($r2);    //rotate by question or student?
            $index1 = substr($index[0],0,1);   //the first char is q or s?
            $index2 = substr($index[0],1);  //get the student id
          ?>
        <option value="Question Number">rotate by question number</option>
        <option value="Student Name">rotate by student name</option>
      </select>
      </div>    
      <div class="col-md-3">
        <select class="form-control" id="qn-bar" name="question_number" disabled>
        <option>Question Number</option>
        <?php
          $user_id = $user['user_id'];
          $q1 = "SELECT temporal_data FROM user WHERE user_id = '$user_id'";
          $r1 = mysqli_query($dbc, $q1);
          $quiz_id = mysqli_fetch_row($r1);

          $q = "SELECT * FROM question WHERE quiz_id = $quiz_id[0]";
          $r = mysqli_query($dbc, $q);

          while ($Question_list = mysqli_fetch_assoc($r)) { ?>

          <option value="<?php echo $Question_list['question_id']; ?>"><?php echo $Question_list['question_number']; ?></option>
          
        <?php } ?>
      </select>
    </div>
      <div class="col-md-3">
        <select class="form-control" id="sn-bar" name="student_name" disabled>
        <option>Student Name</option>
        <?php
          $user_id = $user['user_id'];
          $q = "SELECT * FROM user_course AS uc LEFT JOIN user AS u on uc.user_id = u.user_id WHERE course_id = (SELECT td3 FROM user WHERE user_id = $user_id)";
          $r = mysqli_query($dbc, $q);

          while ($Student_list = mysqli_fetch_assoc($r)) { ?>

          <option value="<?php echo $Student_list['user_id']; ?>"><?php echo $Student_list['first_name']." ".$Student_list['last_name']?></option>
          
        <?php } ?>
      </select>
    </div>
      <div class="col-md-1">
        <button type="submit" data-placement="bottom" data-content="Select the style you want to grade first." id="how_to_grade" formmethod="post" class="btn btn-danger" name="go" style="color:black"><i class="fa fa-play"></i></button>
      </div>
  </form>
</div>

<!-- ......................body below the selection bar......................... -->
<form id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false" style="width:100%; margin: 0 auto">

  <ol class="carousel-indicators" id="label3" data-placement="left" data-content="Green button means you haven't grade this question." style="bottom:-30px;">  
    <?php 
      $user_id = $user['user_id'];
      $q2 = "SELECT td2 FROM user WHERE user_id = '$user_id'";
      $r2 = mysqli_query($dbc, $q2);
      $index = mysqli_fetch_row($r2);    //rotate by question or student? 
      $index2 = substr($index[0],1);  //get the student id
      $i = 0;
      //echo $index2;
      if(substr($index[0],0,1) == 'q')
      {
        //echo $index2;
        $q1 = "SELECT * FROM question AS Q RIGHT JOIN user_question AS U on Q.question_id = U.question_id WHERE U.quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id') AND U.user_id = $index2";
      }
      elseif(substr($index[0],0,1) == 's') {
        $q1 = "SELECT * FROM question AS Q RIGHT JOIN user_question AS U on Q.question_id = U.question_id WHERE U.quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id') AND U.question_id = $index2";
      }

      $r1 = mysqli_query($dbc, $q1);

      while ($my_list = mysqli_fetch_assoc($r1)) {?>
        <li data-target="#myCarousel" id="<?php echo "bot".$my_list['question_id']; ?>" style="background-color:<?php if($my_list['points'] == NULL) echo "green"; else echo "black"; ?>;" data-slide-to="<?php echo "$i"; $i++; ?>" class="<?php if($i == 1){echo "active"; } ?>"></li>
      <?php } ?>
  </ol>

  <div class="carousel-inner">

    <?php 
      $user_id = $user['user_id'];
      $q2 = "SELECT td2 FROM user WHERE user_id = '$user_id'";
      $r2 = mysqli_query($dbc, $q2);
      $index = mysqli_fetch_row($r2);    //rotate by question or student? 
      $index2 = substr($index[0],1);  //get the student id
      //echo $index2;
      if(substr($index[0],0,1) == 'q')
      {
        $q1 = "SELECT * FROM question AS Q RIGHT JOIN user_question AS U on Q.question_id = U.question_id WHERE U.quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id') AND U.user_id = $index2";
      }
      elseif(substr($index[0],0,1) == 's') {
        $q1 = "SELECT * FROM question AS Q RIGHT JOIN user_question AS U on Q.question_id = U.question_id WHERE U.quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id') AND U.question_id = $index2";
      }
      $q4 = "SELECT temporal_data FROM user WHERE user_id = $user_id";
      $r4 = mysqli_query($dbc,$q4);
      $quiz_id = mysqli_fetch_row($r4);

      $r1 = mysqli_query($dbc, $q1);
      $i = 1;
      while ($my_list = mysqli_fetch_assoc($r1)) {
        // $my_question_list = select_my_list($dbc, 'question', 'question', $my_list['question']);
        $my_image_list = select_my_list($dbc, 'question', 'image', $my_list['image']); ?>
          
          <div class="item<?php if($i==1){echo " active"; $i++;} ?>">
            <img src="images/untitled.jpg" alt="First slide" id="background-image" >

            <form>

              <div class="container">
                  <div class="carousel-caption" id="question-image">
                    <?php
                      $student_id = $my_list['user_id'];
                      $q5 = "SELECT first_name, middle_name, last_name FROM user WHERE user_id = $student_id";
                      $r5 = mysqli_query($dbc,$q5);
                      $student_name = mysqli_fetch_row($r5);
                      if($my_list['image'] == 'no image') {?>           <!-- if the question doesn't include an image, use default image -->
                        <img src="images/untitled.jpg" width="<?php echo get_preview_size("untitled.jpg",0,"width", "G"); ?>" height="<?php echo get_preview_size("untitled.jpg",0,"height", "G"); ?>">
                    <?php 
                      } else {?>   <!-- if the there is an image -->
                        <img src="images/<?php echo $quiz_id[0]; ?>/<?php echo $my_list['image'];?>" width="<?php echo get_preview_size($my_image_list['my_index'], $quiz_id[0], "width", "G"); ?>" height="<?php echo get_preview_size($my_image_list['my_index'], $quiz_id[0], "height", "G"); ?>">
                    <?php } ?>
                    <textarea rows="6" readonly><?php echo $my_list['question_number'].". ".$my_list['question']; ?></textarea></br>
                    <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $my_list['question_id']; ?>"> <!-- find the ID of the question -->
                    <!-- student answer -->
                    <textarea rows="7" id="student-answer" name="student_answer"><?php //echo $student_name[0]." ".$student_name[1]." ".$student_name[2].": "; ?><?php echo $my_list['student_answer']; ?></textarea>
                        
                  </div> 
              </div>

              <div class="grade-box"> <!-- grade box -->
                Points:
                <?php
                  $i = 0;
                  while($i <= $my_list['q_point']) { ?>
                    <input class="my-radio" type="radio" <?php if($i == $my_list['points']) echo "checked=\"checked\""; ?> id="<?php echo $my_list['question_id']; ?>" name="<?php echo $my_list['user_id']; ?>" title="<?php echo $user_id; ?>" value="<?php echo $i; ?>"><?php echo $i; $i++; ?>
                <?php }?> 
              </div>
              <?php
              // $q3 = "SELECT SUM(points) FROM user_question WHERE user_id = '8' AND quiz_id = '$quiz_id[0]'";
              // $r3 = mysqli_query($dbc, $q3);
              // $quiz_grade = mysqli_fetch_row($r3);
              // echo $quiz_grade[0];
              //mysqli_query($dbc,"UPDATE user_quiz SET point = '$quiz_grade[0]' WHERE quiz_id = '$quiz_id[0]' AND user_id = '$user_id'");
              ?>
            </form>

          </div>
       
      <?php } ?>
    
  </div>

  <a class="left carousel-control" id="slide_prev" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
  <a class="right carousel-control" id="slide_next" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
  
<!-- <button type="submit" name="save_answer" class="btn btn-vquiz" formmethod="post">Submit</button> -->

</form>
</body>