<!-- 
name: create course
author: Paul
function: Professors can manage quizzes in this page.

modification log:  
-->

<!DOCTYPE html>

<html>
	<head>
		<?php
			include('config/setup.php');
			include('template/check-user.php');
		?>
	</head>
	<body>
		<div id="wrap">
			<?php include('template/navigation.php'); ?>

			<div class="container">
				<h1>Manage course</h1>
				</br></br>

				<?php include('template/create-course/create-course-button.php'); ?>
	
			</div>
		</div>

		<?php include('template/footer.php'); ?>
	</body>
</html>