<?php 
	include('setup.php');
	

?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page['page_title']. ' | ' .$site_title; ?></title>
   	<?php include('spare-css.php');?>
	<?php include('js.php');?>


  </head>
  <body>
  	<div id="wrap">
  	<header>
	  	<nav class="navbar navbar-default">        
	  		<ul class="nav navbar-nav">
          <?php //<?php nav_main($dbc, $pageid); ?>
	  			<li><a href="index.php">Dashboard</a></li>
	  			<li><a href="createquiz.php">Create Quiz</a></li>
	  			<li><a href="takequiz.php">Take Quiz</a></li>
          <li><a href="grade-quizzes.php">Grade Quizzes</a></li>
          <li><a href="results.php">Results</a></li>
	  		</ul>
        <div class="user-buttons btn-group">
          
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i> 
            <?php echo $user['fullname'];?>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
             <ul class="dropdown-menu" role="menu">
              <!-- <li><button id="btn-debug" class="btn btn-primary navbar-btn"><i class="fa fa-bug"></i> Debug</button></li> -->
              <li><a href="logout.php"><button class="btn btn-primary navbar-btn"><i class="fa fa-sign-out"></i> Logout</button></a></li>
            </ul>
      </div>
	  	</nav>
  	</header>
  	<div class="container">