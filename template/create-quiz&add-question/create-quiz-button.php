<?php 
	include('css/create-quiz/button-css.php'); 
	include('functions/create-quiz&add-question/postback.php');
	include('js/create-quiz&add-question/create-quiz-js.php');
	include('js/create-quiz&add-question/delete-quiz-js.php');
?>
<form>
	<div class="col-md-2">
		<select class="form-control" name="course_list">												
			<?php 
				$user_id = $user['user_id'];
				$q = "SELECT * FROM course";
				$r = mysqli_query($dbc, $q);

				$q1 = "SELECT td3 FROM user WHERE user_id = '$user_id'";
				$r1 = mysqli_query($dbc, $q1);
				$course_id = mysqli_fetch_row($r1);
				while ($Course_list = mysqli_fetch_assoc($r)) {
					//$Image_list = select_my_list($dbc, 'image', 'image', $Image_list['image']);
				?>
				<option value="<?php echo $Course_list['course_id']; ?>" <?php if($Course_list['course_id'] == $course_id[0]) echo "selected=\"selected\""; ?>><?php echo $Course_list['course_name']." ".$Course_list['section_number']; ?></option>

			<?php } ?>
		</select>
	</div>
	<button type="submit" name="select_course" formmethod="post" class="btn btn-default">go</button>
</form>

<?php
	$user_id = $user['user_id'];
	$q1 = "SELECT * FROM quiz WHERE course_id = (SELECT td3 FROM user WHERE user_id = $user_id)";
	$r1 = mysqli_query($dbc, $q1);

	while($Quiz_list = mysqli_fetch_assoc($r1) ) { ?> 
		<div class="col-md-4">
			<form>	
				<input type="hidden" name="quiz_id" readonly="readonly" value="<?php echo $Quiz_list['quiz_id']; ?>">

	<!-- 			<button id="mySCB" type="submit" onclick="selectQuiz()"></button> -->
				<input type="submit" formmethod="post" name="edit_quiz" value="<?php echo $Quiz_list['quiz_name']." ".$Quiz_list['quiz_date']; ?>" id="<?php echo $Quiz_list['quiz_id']; ?>" class="mySCB">
				<button id="deleteButton" name="<?php echo $Quiz_list['quiz_id']; ?>" class="deleteButton" style="position:absolute; top:10px; right:15px;"><i class="fa fa-times"></i></button>
			</form>	
		</div>
<?php } ?>  
<form>
	<div class="col-md-4">
		<button class="mySCB" name="add_quiz" type="submit" formmethod="post"><i class="fa fa-plus fa-4x"></i></button> <!-- myCCB: my select course button -->
	</div>
</form>

<!-- 
name: select courses button
author: Paul
function: Select a quiz before you add questions.
 
modification log:  
18:16 9/1/2014 finally,finally...now i can get the button id and store it in database by using ajax.
19:00 10/1/2014 quizzes can be deleted now.
-->
