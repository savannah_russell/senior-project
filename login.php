<!-- 
name: login
author: Paul & Savannah
function: People start to login in this page.
 
modification log:
14:10 6/30/2014 add sign up button.
-->



<?php
	include('config/connection.php');
	include('template/login.php');
	include('css/css.php'); 
	include('css/login-css.php');
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Log In</title>
	</head>
	<body >
	 	<div id="wrap">
		    <div class="container">
		    	<div class="row">
		    	</br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>

					<div class="col-md-6 col-md-offset-3">	
						<div class="panel panel-info">
							<div class="panel-heading">
						    	
						  	</div>
						  	<div class="panel-body">  
						    	<form action="login.php" method="post" role="form">

						      		<div class="form-group">
								        <label for="email">Email address</label>
								        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
						      		</div>
						      		<div class="form-group">
								        <label for="password">Password</label>
								        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
						      		</div>
						      		<div class="form-group">
						      			<button type="submit" name="login" class="btn btn-primary">Login</button>
						    			<button type="submit" name="sign_up" class="btn btn-danger">Sign up</button>
						      		</div>	
						    	</form>
						  	</div>
						  	<div class="panel-heading">
						    	
						  	</div>	
						</div><!-- end panel -->
					</div>
					

					
				</div><!-- end row -->
				

			</div>
		</div>
	 	<?php //include('template/footer.php'); ?> 
	</body>
</html>