-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2014 at 10:49 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vqbuild`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer_text` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `answer_grade` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `question_id`, `user_id`, `answer_text`, `answer_grade`) VALUES
(1, 1, 1, 'No', ''),
(2, 2, 1, 'Nope', ''),
(3, 3, 1, 'Yes', ''),
(4, 4, 1, 'Nobody gets left behind.', ''),
(5, 1, 2, 'fhqwhgads', ''),
(6, 4, 4, 'everybody to the limit', ''),
(7, 4, 5, 'I aim to misbehave', ''),
(8, 4, 7, 'Shiny!', ''),
(9, 4, 7, 'Machines just got workings, and they talk to me', ''),
(10, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `course_number` int(11) NOT NULL,
  `course_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `department`, `course_number`, `course_name`) VALUES
(1, 'BIO', 142, 'Cuttin'' Up Stuff'),
(2, 'BIO', 355, 'Cuttin'' Up More');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`image_id`, `image`) VALUES
(1, 'panda.jpg'),
(2, 'elephants.jpg'),
(4, 'Untitled.jpg'),
(5, '1243732437237.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(9) NOT NULL,
  `page_label` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `page_header` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `page_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`page_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `user_id`, `page_label`, `page_title`, `page_header`, `page_content`, `page_url`) VALUES
(1, 1, 'Home', 'Home Page', 'Welcome to VisQuiz!', '<p><strong>Lucas</strong> ipsum dolor sit amet moff wampa solo jinn solo c-3p0 jabba zabrak luuke solo. Han jabba moff qui-gonn skywalker dooku windu tatooine moff. Antilles hutt kamino mustafar fisto. Vader ahsoka chewbacca jinn cade lars dooku organa. Qui-gon twi''lek calrissian mara. Cade endor sidious wookiee darth qui-gonn wicket. Twi''lek maul ackbar moff. Mon utapau moff wedge k-3po solo dantooine. Maul jade jabba jade. Luke mon organa dantooine coruscant. Maul obi-wan jade antilles jade antilles tatooine ventress moff.</p>\r\n\r\n<p>Darth alderaan sidious jabba binks lobot antilles. Yavin naboo bespin yoda darth ben. Ackbar anakin darth fett organa ben zabrak mustafar organa. Twi''lek skywalker kenobi solo padme mara skywalker. Vader jango mace dooku wicket. Obi-wan wicket fett yoda. Anakin darth organa hutt. Antilles leia fisto han calamari secura. Hoth antilles ahsoka antilles sith. Grievous darth qui-gon moff fett yoda yavin darth qui-gon. Hutt mon darth calamari obi-wan hutt. Palpatine yoda luuke luuke. Tusken raider leia hutt alderaan hutt sidious palpatine mandalore padme.</p>', 'home'),
(2, 2, 'About', 'About Us', 'About VisQuiz', 'carboy craft beer bung seidel crystal malt, bacterial. wort chiller aau top-fermenting yeast, bittering hops. bitter units of bitterness original gravity dry hopping. cask conditioning dextrin microbrewery pitch amber balthazar; hydrometer. barleywine dunkle fermentation enzymes. shelf life length bacterial alcohol bittering hops. pint glass primary fermentation cask conditioning tulip glass aerobic degrees plato brewpub.\r\n\r\nbalthazar amber terminal gravity autolysis ibu. biere de garde crystal malt units of bitterness aerobic sparge bright beer. infusion craft beer wort chiller racking pitching cask conditioning alcohol draft (draught) bock. attenuation pitch terminal gravity, cask conditioning abbey pilsner wort. dry stout sparge cask conditioning. oxidized length malt extract filter. bock additive bock barrel berliner weisse. noble hops terminal gravity brew squares lambic specific gravity. enzymes grainy, krausen cask.', 'about'),
(3, 2, 'Test', 'Testing', 'Testing testing hello', ';alkfjds;lkfj;dlkfdlkafj;sdlkfjds;lkasjd', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `question_number` int(11) NOT NULL,
  `question_text` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `answer_key` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`question_id`,`quiz_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`question_id`, `quiz_id`, `question_number`, `question_text`, `answer_key`, `image`) VALUES
(1, 1, 1, 'hello', 'hi', 'panda.jpg'),
(2, 1, 2, 'aaa', 'bbb', 'elephants.jpg'),
(3, 1, 3, 'What is thy quest?', 'We seek the grail', 'panda.jpg'),
(4, 0, 0, '', '', ''),
(5, 0, 0, '', '', ''),
(6, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quiz_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `direction` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`quiz_id`,`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `section_id`, `user_id`, `quiz_title`, `direction`) VALUES
(1, 1, 1, 'Quizzin'' a Quiz', 'Take this quiz please');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `max_students` int(11) NOT NULL,
  `q1_correct` int(11) NOT NULL,
  `q2_correct` int(11) NOT NULL,
  `q3_correct` int(11) NOT NULL,
  `q4_correct` int(11) NOT NULL,
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`result_id`, `quiz_id`, `max_students`, `q1_correct`, `q2_correct`, `q3_correct`, `q4_correct`) VALUES
(1, 4, 50, 35, 23, 10, 50);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `section_number` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `instructor_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `semester` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`section_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `label`, `value`) VALUES
('debug-status', 'Debug Status', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `first_name`, `last_name`, `email`, `status`, `role`) VALUES
(1, 'piercesoft', 'password', 'Pierce', 'Software', 'pierce.software.drury@gmail.com', '1', ''),
(2, 'piercesoft2', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Pierce', 'Software', 'admin@piercesoft.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_question`
--

CREATE TABLE IF NOT EXISTS `user_question` (
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_quiz`
--

CREATE TABLE IF NOT EXISTS `user_quiz` (
  `user_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_section`
--

CREATE TABLE IF NOT EXISTS `user_section` (
  `user_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
