<!-- 
name: carousel take quiz
author: Paul & Savannah
function: Set up the carousel in take quiz page.
 
modification log:
23:52 6/10/2014 working on the textarea.
-->

<?php 
  include('css/take-quiz/carousel-css.php'); 
  include('functions/take-quiz/postback.php');
  include('functions/get-preview-size.php');
  include('js/take-quiz/save-answer-js.php');
  include('js/take-quiz/tutorial.php');
?>
<?php
  $user_id = $user['user_id'];
  $q2 = "SELECT * FROM quiz WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
  $r2 = mysqli_query($dbc, $q2);
  $quizInfo = mysqli_fetch_row($r2);
?>

<h1 style="display:inline">Take Quiz: <?php echo $quizInfo[2]; ?>

<button type="button" class="btn btn-success" style="float:right" id="tutorial2">Tutorials</button></h1>
<label style="display:hidden;float:right" id="label2" data-placement="left" data-content="Click it again to turn off tutorials." for="tutorial2"></label>
<form id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false" style="width:100%; margin: 0 auto">
   
  <ol class="carousel-indicators" id="label3" data-placement="left" data-content="Green button means you haven't answer this question.">
    <?php 
      $q = "SELECT * FROM question WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";
      $r = mysqli_query($dbc, $q);
      $i = 0;

      while ($my_list = mysqli_fetch_assoc($r)) {
        $question_id = $my_list['question_id'];
        $q3 = "SELECT * FROM user_question WHERE question_id = $question_id AND user_id = $user_id";
        $r3 = mysqli_query($dbc, $q3); 
        $SA = mysqli_fetch_row($r3); ?>
       
        <li data-target="#myCarousel" id="<?php echo "bot".$question_id; ?>" name="<?php echo "botButton".$question_id ?>" style="background-color:<?php if ($SA[4] != NULL) echo "black"; else echo "green"; ?>" data-slide-to="<?php echo $i; $i++; ?>" class="<?php if($i == 1){echo "active"; } ?>"></li>
      <?php } ?>
  </ol>
  <div class="carousel-inner">

    <?php
      $user_id = $user['user_id'];
      
      $q4 = "SELECT temporal_data FROM user WHERE user_id = $user_id";
      $r4 = mysqli_query($dbc,$q4);
      $quiz_id = mysqli_fetch_row($r4);

      $q1 = "SELECT * FROM question WHERE quiz_id = $quiz_id[0]";
    	$r1 = mysqli_query($dbc, $q1);
      $i = 1;
    	while ($my_list = mysqli_fetch_assoc($r1)) {
    		// $my_question_list = select_my_list($dbc, 'question', 'question', $my_list['question']);
        $my_image_list = select_my_list($dbc, 'question', 'image', $my_list['image']); ?>
        <div class="item<?php if($i==1){echo " active"; $i++;} ?>">
          <img src="images/untitled.jpg" alt="First slide" id="background-image" >

          <div class="container">
            <div class="carousel-caption" id="question-image">
              <?php 
                if($my_list['image'] == 'no image') {?>           <!-- if the question doesn't include an image, use default image -->
                  <img src="images/untitled.jpg" width="<?php echo get_preview_size("untitled.jpg",0,"width", "T"); ?>" height="<?php echo get_preview_size("untitled.jpg",0,"height", "T"); ?>">
              <?php 
                } else {?>   <!-- if the there is an image -->
                  <img src="images/<?php echo $quiz_id[0]; ?>/<?php echo $my_list['image'];?>" width="<?php echo get_preview_size($my_image_list['my_index'], $quiz_id[0], "width", "T"); ?>" height="<?php echo get_preview_size($my_image_list['my_index'], $quiz_id[0], "height", "T"); ?>">
              <?php } ?>
              <textarea rows="3" id="question" readonly><?php echo $my_list['question_number'].". ".$my_list['question']; ?></textarea>

              <!-- if the student has answered this question, print out the answer-->
              <?php
                $question_id = $my_list['question_id'];
                $q3 = "SELECT student_answer FROM user_question WHERE user_id = $user_id AND question_id = $question_id";
                $r3 = mysqli_query($dbc,$q3);
                $student_answer = mysqli_fetch_row($r3);
              ?>
              <textarea rows="3" class="form-control" id="<?php echo "s_a".$my_list['question_id']; ?>"><?php echo $student_answer[0];?></textarea>

              <button type="button" class="btn btn-danger" id="<?php echo $my_list['question_id']?>" name="<?php echo $user_id; ?>">save</button>
              <label style="display:hidden" id="save_button" data-placement="right" data-content="Please click save for each question!"></label>           
    <!-- default type value is submit inside form tag -->
            </div>
            
          </div>
        </div>
    	<?php } ?>

  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
	
<!-- <button type="submit" name="save_answer" class="btn btn-vquiz" formmethod="post">Submit</button> -->
			
</form>

