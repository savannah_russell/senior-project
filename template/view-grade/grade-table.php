<!-- 
name: grade table
author: Paul
function: Professor view all students grade.
 
modification log:

-->
<table class="table table-striped">
	<?php 
		$question_id_array = array();  //make sure the question num sequence is 1,2,3,.....
		$total_point = 0;
	?>  
	<thead>
		<tr>	<!-- first row -->
			<th>Student name</th>
			<?php
				$user_id = $user['user_id'];
				$q = "SELECT question_number, question_id, q_point FROM question WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id')";
				$r = mysqli_query($dbc, $q);
				while ($question_list = mysqli_fetch_array($r)) { ?>
					<th><?php echo $question_list[0]."($question_list[2] pt)"; $question_id_array[]=$question_list[1]; ?></th>  
			<?php } ?>
			<th>Total</th>

		</tr>
	</thead>
	<tbody>			<!-- rest of the row -->
		<?php
			$user_id = $user['user_id'];
			$user_status = $user['status'];

			if($user_status == 'student') { 
				$q = "SELECT * FROM user_quiz AS Q LEFT JOIN user AS U on Q.user_id = U.user_id WHERE Q.quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id') AND Q.user_id = $user_id";
			} else {
				$q = "SELECT * FROM user_quiz AS Q LEFT JOIN user AS U on Q.user_id = U.user_id WHERE Q.quiz_id = (SELECT temporal_data FROM user WHERE user_id = '$user_id')";
			}
			$r = mysqli_query($dbc, $q);
			$i = 1;
			while ($grade_list = mysqli_fetch_assoc($r)) { ?>
			<?php if($i == 1) { ?>
				<tr class="info">
					<td><?php echo $grade_list['last_name'].", ".$grade_list['first_name']." ".$grade_list['middle_name']; ?></td>
					<?php
						$total_point = 0;
						for($i = 0; $i < count($question_id_array); $i++)   // each question's point with this student
						{
							$student_id = $grade_list['user_id'];
							$question_id = $question_id_array[$i];

							$q1 = "SELECT points FROM user_question WHERE user_id = $student_id AND question_id = $question_id";
							$r1 = mysqli_query($dbc, $q1);
							$point_for_each_question = mysqli_fetch_row($r1);

							$total_point += $point_for_each_question[0];
							echo "<td>".$point_for_each_question[0]."</td>";
						}
						echo "<td>".$total_point."</td>";
					?>
				</tr>
			<?php $i = -1;} elseif($i == -1) { ?>
				<tr class="danger">
					<td><?php echo $grade_list['last_name'].", ".$grade_list['first_name']." ".$grade_list['middle_name']; ?></td>
					<?php
						$total_point = 0;
						for($i = 0; $i < count($question_id_array); $i++)   // each question's point with this student
						{
							$student_id = $grade_list['user_id'];
							$question_id = $question_id_array[$i];

							$q1 = "SELECT points FROM user_question WHERE user_id = $student_id AND question_id = $question_id";
							$r1 = mysqli_query($dbc, $q1);
							$point_for_each_question = mysqli_fetch_row($r1);

							$total_point += $point_for_each_question[0];
							echo "<td>".$point_for_each_question[0]."</td>";
						}
						echo "<td>".$total_point."</td>";
					?>
				</tr>
			<?php $i = 1;} ?>
		<?php } ?>
	</tbody>
</table>