<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		$("#tutorial1").click(function() {
			$("#go").popover('toggle');
			$('#select_quiz').popover('toggle');
			$('#availability').popover('toggle');
			$('#deleteButton').popover('toggle');
			$('#add_quiz').popover('toggle');
			$('#label1').popover('toggle');
		});

		$("#tutorial2").click(function() {
			$("#total_point").popover('toggle');
			$('#upload_image').popover('toggle');
			$('#percent_of_final').popover('toggle');
			$('#label2').popover('toggle');
		});
	});


</script>