

<!--Jquery UI CSS-->
	<script src="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"></script>
	<!--Jquery, Jquery UI, minified JS-->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
<!-- jQuery -->
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<!-- jQuery UI -->
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script src="http://code.highcharts.com/highcharts.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>


<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<script>
  
  $(document).ready(function() {
    
    $("#console-debug").hide();
    
    $("#btn-debug").click(function(){
      
      $("#console-debug").toggle();
      
    });
    
    
  });

var counter = 1;
var limit = 0; // Make limit > 0 to have a maximum question count.
function addQuestion(divName){
     if (counter == limit)  {
        alert("You can only enter up to " + limit + "questions.");
     }
     else {
          var newdiv = document.createElement('div');
          newdiv.innerHTML = getOutput();
          document.getElementById(divName).appendChild(newdiv);
          counter++;
     }
}
  


function getOutput() {
  getRequest(
      'accordioner.php', // URL for the PHP file
       drawOutput,  // handle successful request
       drawError    // handle error
  );
  return false;
}  
// handles drawing an error message
function drawError () {
    var container = document.getElementById('question_block');
    container.innerHTML = 'Bummer: there was an error!';
}
// handles the response, adds the html
function drawOutput(responseText) {
    var container = document.getElementById('question_block');
    container.innerHTML = responseText;
}
// helper function for cross-browser request object
function getRequest(url, success, error) {
    var req = false;
    try{
        // most browsers
        req = new XMLHttpRequest();
    } catch (e){
        // IE
        try{
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try{
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e){
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {};
    if (typeof error!= 'function') error = function () {};
    req.onreadystatechange = function(){
        if(req .readyState == 4){
            return req.status === 200 ? 
                success(req.responseText) : error(req.status)
            ;
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}  



</script>

