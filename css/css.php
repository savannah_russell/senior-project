<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

<style>
	html,body {
		height: 100%;
	}
	#wrap {
		min-height: 100%;
		height: auto;
		margin: 0 auto -20px;
		padding: 0 0 60px;
	}
	#footer{
		height: 20px;
		background-color: #f5f5f5;
	}

	.btn-vquiz {
		background-color: #1abc9c;
		color: #ffffff;
	}/*set button style*/
	
	.panel-info>.panel-heading{
		color: #ffffff;
		background-color: #1abc9c;
		background-image: none;
	}/*set panel style*/

	h1 {
	    margin-top: 0px;
	    margin-bottom: 0px;
	}

	p {
    	margin-bottom: 0px;
	}
	.popover {
    	background: tomato;
	}
</style>

