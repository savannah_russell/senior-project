<?php	
	include('css/create-quiz/button-css.php'); 
	include('functions/view-grade/postback.php');
	include('js/view-grade/export-grade-js.php');
	include('js/view-grade/tutorial.php');
?>
<form>
	<div class="col-md-2">
		<select class="form-control" name="course_list">												
			<?php 
				$user_id = $user['user_id'];
				$user_status = $user['status'];

				if($user_status == 'professor') {
					$q = "SELECT * FROM course WHERE instructor_id = $user_id";
				} elseif ($user_status == 'student') {
					$q = "SELECT * FROM course RIGHT JOIN user_course on course.course_id = user_course.course_id WHERE user_id = $user_id";
				}
				
				$r = mysqli_query($dbc, $q);

				$q1 = "SELECT td3 FROM user WHERE user_id = '$user_id'";
				$r1 = mysqli_query($dbc, $q1);
				$course_id = mysqli_fetch_row($r1);
				while ($Course_list = mysqli_fetch_assoc($r)) {
					//$Image_list = select_my_list($dbc, 'image', 'image', $Image_list['image']);
				?>
				<option value="<?php echo $Course_list['course_id']; ?>" <?php if($Course_list['course_id'] == $course_id[0]) echo "selected=\"selected\""; ?>><?php echo $Course_list['course_name']." ".$Course_list['section_number']; ?></option>

			<?php } ?>
		</select>
	</div>
	<button type="submit" name="select_course" data-placement="top" data-content="You need to select a course first!" id="go" formmethod="post" class="btn btn-danger">go</button>
	<label style="display:hidden;float:right" id="label1" data-placement="top" data-content="Click it again to turn off tutorials." for="tutorial1"></label>
	<button type="button" class="btn btn-success" style="float:right" id="tutorial1">Tutorials</button>

	<?php 
		if($user_status == 'professor') {?>
			<button type="submit" formmethod="post" id="export_grade" title="<?php echo $user_id; ?>" name="export_grade" class="btn btn-danger"><i class="fa fa-download"></i> export all quizzes record</button>
	<?php } ?>
</form>

<?php
	$user_id = $user['user_id'];
	$q1 = "SELECT * FROM quiz WHERE course_id = (SELECT td3 FROM user WHERE user_id = $user_id)";
	$r1 = mysqli_query($dbc, $q1);

	while($Quiz_list = mysqli_fetch_assoc($r1) ) { ?> 
		<div class="col-md-4">
			<form>	
				<input type="hidden" name="quiz_id" readonly="readonly" value="<?php echo $Quiz_list['quiz_id']; ?>">

	<!-- 			<button id="mySCB" type="submit" onclick="selectQuiz()"></button> -->
				<input type="submit" formmethod="post" data-placement="right" data-content="Click to get into the quiz." name="select_quiz" value="<?php echo $Quiz_list['quiz_name']." ".$Quiz_list['quiz_date']; ?>" id="select_quiz" class="mySCB">
			</form>	
		</div>
<?php } ?> 