<style type="text/css">

.my-grade-bar {
	display: block;
	width: 85%;
	height: 34px;
	padding: 6px 12px;
	font-size: 14px;
	line-height: 1.42857143;
	color: #555;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
}

#save-button {
	width: 45%;
}

.grade-button {
	background-image: linear-gradient(to bottom,#f0ad4e 0,#eb9316 100%);
	background-repeat: repeat-x;
	border-color: #e38d13;
	border: 1px solid transparent;
	box-shadow: inset 0 1px 0 rgba(255,255,255,.15),0 1px 1px rgba(0,0,0,.075);
	padding: 6px 12px;
	line-height: 1.42857143;
	border-radius: 4px;
	text-shadow: 0 -1px 0 rgba(0,0,0,.2);
}

</style>
