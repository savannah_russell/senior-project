<!-- 
name: navigation
author: Paul & Savannah
function: Set up the navigation bar.
 
modification log: 
20:20 9/9/2014 comments should not put in the postback.php 
-->

<?php 
	include('css/navbar-css.php');
?>


<nav class="navbar navbar-default">
    <div class="container">
		<ul class="nav navbar-nav">
<!-- 			<li><a href="#">Dashboard</a></li>
			<li><a href="create-course.php">Course</a></li>
			<li><a href="select-quiz-b4-create-q.php">Create Quiz</a></li>
			<li><a href="select-quiz-b4-take-q.php">Take Quiz</a></li>
			<li><a href="select-quiz-b4-grade-q.php">Grade Quiz</a></li>
			<li><a href="select-quiz-b4-view-g.php">View Grade</a></li>
			<li><a href="sign-up-course.php">Sign Up Course</a></li> -->
			<?php include('functions/nav_main.php'); ?>
		</ul>

		<div class="user-buttons btn-group">
		    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
		    	<i class="fa fa-user"></i> 
		        <?php echo $user['fullname'];?>
		        <span class="caret"></span>
		        <span class="sr-only">Toggle Dropdown</span>
		    </button>
		    <ul class="dropdown-menu" role="menu">
		    	<li><a href="logout.php"><button class="btn btn-primary navbar-btn"><i class="fa fa-sign-out"></i> Logout</button></a></li>
		    </ul>
		</div>
	</div>

</nav>
