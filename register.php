<?php 

session_start();
 
 include('setup.php');?>

  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo 'Register | ' .$site_title; ?></title>
    <?php include('spare-css.php');?>
  <?php include('js.php');?>

  </head>
  <body>
 	<div id="wrap">
    <div class="container">
<?php // Insert registered info into database.
		
			if(isset($_POST['submitted'])) {			

				
	
					
					$q = "INSERT INTO user (first_name, last_name, username, email, password) VALUES ('$_POST[first_name]', '$_POST[last_name]', '$_POST[username]', '$_POST[email]', SHA1('$_POST[password]'))";
					$r = mysqli_query($dbc, $q);
					
					if($r) {
						$user = data_user($dbc, $email);
						$_SESSION['email'] = $_POST['email'];
						header('Location: admin/login.php');
						
					} else {
						$message = '<p>User could not be registered because: '.mysqli_error($dbc).'</p>';
						$message .= '<p>'.$q.'</p>';
					}
				
			}		
		?>
<div id="wrap">
    <div class="container">

<div class="panel panel-info">
  <div class="panel-heading">
    <h1>Register</h1>
  </div>

  <div class="panel-body">
			<div class="row">	
								
					<?php
					
						if(isset($message)) {
								echo '<div class="col-md-6 col-md-offset-3">'.$message.'</div></div>';
							
						} else { // If user is created or an error occurs, display message.  Otherwise, show registration form.				
					
					?>
				
								
				<div class="col-md-3 col-md-offset-3">
					
				<form action="register.php" method="post" role="form">
					  <div class="form-group">
					    <label for="first_name">First Name:</label>
					    <input type="text" class="form-control user-function" id="first_name" name="first_name" placeholder="First Name"> 
					  </div>
					  
					</div>
				
					<div class="col-md-3">
					  
					  <div class="form-group">
					    <label for="last_name">Last Name:</label>
					    <input type="text" class="form-control user-function" id="last_name" name="last_name" placeholder="Last Name">
					  </div>
					
					</div>
					
									
					<div class="col-md-6 col-md-offset-3" style="margin-bottom: 10px;">

						  <div class="form-group">
						    <label for="username">Username:</label>
						    <input type="text" class="form-control user-function" id="username" name="username" placeholder="Username"> 
						  </div>

						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" class="form-control user-function" id="email" name="email" placeholder="Email"> 
						  </div>
						  
						  <div class="form-group">
						    <label for="password">Password:</label>
						    <input type="password" class="form-control user-function" id="password" name="password" placeholder="Password"> 
						  </div>
						  
						  <button type="submit" class="btn btn-vquiz">Register</button>
						  <input type="hidden" name="submitted" value="user_validate_prepare">
					</form>
					
					<?php } ?> 
		
				</div> 
				
			</div> 
			</div>
</div>

<?php include('theme_1/footer.php'); ?>