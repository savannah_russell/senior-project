<?php 
#Start session
session_start();
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}

?>

<?php include('theme_1/header.php'); ?>
 	
<h1>Admin Dash</h1>

<div class="row">
	<div class="col-md-3">
		
	</div>
	<div class="col-md-9">
		<table class="table table-striped">

			<tbody>
				
				<tr>
					<th>User</th>
					<th>Class</th>
					<th>Quiz Title</th>
				</tr>
				
				
						<?php 

						$q = "SELECT * FROM quizzes ORDER BY class_code ASC";
						$r = mysqli_query($dbc, $q);

						while ($quiz_list = mysqli_fetch_assoc($r)) { ?>
							
						<tr>
							<td><?php echo $quiz_list['user_id'];?></td>
							<td><?php echo $quiz_list['class_code'];?></td>
							<td><?php echo $quiz_list['quiz_title'];?></td>

						</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<?php if($debug == 1) { include('widgets/debug.php'); } ?>

<?php include('theme_1/footer.php'); ?>