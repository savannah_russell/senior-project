<?php 
// name: take quiz
// author: Paul
// function: In this web page, the student can take a quiz.
// 
// modification log: 
// 
#Start session

session_start();
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}

 include('theme_1/header.php'); ?>
<h1>Take Quiz</h1>
<div class="form-group">
	<input type="text" class="form-control" style="width: 25%" name="user_id" placeholder="User ID">
</div>


<form id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
      <!-- Indicators -->
      <ol class="carousel-indicators">  
        <?php 
          $q = "SELECT * FROM question WHERE quiz_id = 1";
          $r = mysqli_query($dbc, $q);
          $i = 0;

          while ($my_list = mysqli_fetch_assoc($r)) {
            $question_list = select_my_list($dbc, 'question', 'question_id', $my_list['question_id']);
          ?>
          <li data-target="#myCarousel" style="background-color:black;" data-slide-to="<?php echo "$i"; $i++; ?>" class="<?php if($i == 1){echo "active"; } ?>"></li>

        <?php } ?>
      </ol>
      <div class="carousel-inner">

        <?php 
        	$q1 = "SELECT * FROM question WHERE quiz_id = 1";
        	$r1 = mysqli_query($dbc, $q1);
            $i = 1;
        	while ($my_list = mysqli_fetch_assoc($r1)) {
        		$my_question_list = select_my_list($dbc, 'question', 'question_text', $my_list['question_text']);
        						
                    //$my_answer_list = select_my_list($dbc, 'question', 'answer', $my_list['answer']);
                $my_image_list = select_my_list($dbc, 'question', 'image', $my_list['image']); ?>
                  <div class="item<?php if($i==1){echo " active"; $i++;} ?>">
                    <img src="upload/untitled.jpg" alt="First slide" id="test" >

                    <div class="container">
                      <div class="carousel-caption" id="test2">
                       
                          <img src="upload/<?php echo $my_image_list['my_index']; ?>">
                          <h1><?php echo $my_question_list['question_text']?></h1>

                          <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $my_list['question_id'] ?>"> <!-- find the ID of the question -->

                          <textarea rows="4" class="form-control" id="texttest" name="student_answer"></textarea>
                          <button type="submit" name="save_answer" style="color:black" formmethod="post">save</button> 
                        
                      </div>
                    </div>
                  </div>
              <!--     <button type="submit" name="save_answer" class="btn btn-vquiz" formmethod="post">Submit</button> -->
        			  <?php } ?>

      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
  
	
<!-- <button type="submit" name="save_answer" class="btn btn-vquiz" formmethod="post">Submit</button> -->
			
		</div>
	</div>
</form>

<?php if($debug == 1) { include('widgets/debug.php'); } ?>	  

<?php include('theme_1/footer.php'); ?>