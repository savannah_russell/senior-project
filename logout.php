<!-- 
name: logout
author: Paul & Savannah
function: The logout page.
 
modification log:  
-->

<?php

session_start();

unset($_SESSION['username']);

header('Location: login.php');

?>