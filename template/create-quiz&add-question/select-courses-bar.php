<!-- 
name: select courses bar
author: Paul
function: Select a course before you add quizzes.
 
modification log:  
-->


<div class="col-md-2">
	<select class="form-control" onchange="reset_option()">												
		<?php 
			$user_id = $user['user_id'];
			$user_status = $user['status'];

			if($user_status == 'professor') {
				$q = "SELECT * FROM course WHERE instructor_id = $user_id";
			} elseif ($user_status == 'student') {
				$q = "SELECT * FROM course RIGHT JOIN user_course on course.course_id = user_course.course_id WHERE user_id = 8";
			}
			
			$r = mysqli_query($dbc, $q);

			while ($Course_list = mysqli_fetch_assoc($r)) {
				//$Image_list = select_my_list($dbc, 'image', 'image', $Image_list['image']);
			?>
			<option value="<?php echo $Course_list['course_id']?>"><?php echo $Course_list['course_name']." ".$Course_list['section_number']?></option>

		<?php } ?>
	</select>
</div>