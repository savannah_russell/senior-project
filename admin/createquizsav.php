<?php 
//How to get this working:
//Three sections: Insert Quiz Info, Upload Image/Insert Image Name, Insert Question Info
//When first section posts, reveal Upload Image and grey out first submit button
//When second section posts, reveal first Question and grey out submit button
//Each Question is own submittable block
//When a question is submitted, grey out its button


#Start session
session_start();
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}

?>
<style>
    		label, input { display:block; }
  		</style>
<?php include('theme_1/header.php'); ?>
	
<h1>Create Quiz</h1>

<div class="row">
	<div class="col-md-5">
		<?php 
		// Insert array into DB, returns fetch_assoc of inserted question info.			

		if(isset($_POST['quiz_submitted']) == 1) {
			//change where these go
			$user_id = mysqli_real_escape_string($dbc, $_POST['user_id']);
			$quiz_title = mysqli_real_escape_string($dbc, $_POST['quiz_title']);
			$section_id = mysqli_real_escape_string($dbc, $_POST['section_id']);
			$direction = mysqli_real_escape_string($dbc, $_POST['direction']);

			
			//Go into Quiz table
			$q = "INSERT INTO quiz (user_id, quiz_title, section_id, direction) VALUES ('$user_id', '$quiz_title', '$section_id', '$direction' )";
			$r = mysqli_query($dbc, $q);

			//Go into Question table
			// $new_question_array = array();
			// $storage1 = "quiz_id, question_id, question_text, answer_key";
			// $storage2 = "'$array[quiz_id]', '$array[question_id]', '$array[question_text]', '$array[answer_key]'";
			// insert_array($dbc, $new_question_array,'question', $storage1, $storage2);
			
			if($r){
				$message = '<p>Quiz Submitted!</p>';
			}
			else {
				$message = '<p>Quiz was not added because '.mysqli_error($dbc).'</p>';
				$message = '<p>'.$q.'</p>';
			}
			
		}
		if(isset($_POST['question_submitted'])== 1){
			$quiz_id = mysqli_real_escape_string($dbc, $_POST['quiz_id']);
			$question_number = mysqli_real_escape_string($dbc, $_POST['question_number']);
			$question_text = mysqli_real_escape_string($dbc, $_POST['question_text']);
			$answer_key = mysqli_real_escape_string($dbc, $_POST['answer_key']);
			$image = mysqli_real_escape_string($dbc, $_POST['image_list']);
			
			$quq = "INSERT INTO question (quiz_id, question_number, question_text, answer_key, image) VALUES ('$quiz_id', '$question_number', '$question_text', '$answer_key', '$image')";
			$rq = mysqli_query($dbc, $quq);
			if($rq){
				$message3 = '<p>Question Submitted!</p>';
			}
			else {
				$message3 = '<p>Question was not added because '.mysqli_error($dbc).'</p>';
				$message3 = '<p>'.$rq.'</p>';
			}

		}
		if(isset($message)) {
			echo $message;
		}
		if(isset($message2)){
			echo $message2;
		}
		if(isset($message3)){
			echo $message3;
		}
		?>
			<form role="form">
				<div class="form-group">
					<input type="hidden" name="user_id" value="2"<?php //This should get the user_id from the $_SESSION?>>
				</div>
				<div class="form-group">
					<label for="section_id">Section ID</label>
					<input type="text" class="form-control" name="section_id" placeholder="<?php if (isset($_POST['section_id'])){echo $_POST['section_id'];}?>">
				</div>
				<div class="form-group">
					<label for="quiz_title">Quiz Title</label>
					<input type="text" class="form-control" name="quiz_title" placeholder="<?php if (isset($_POST['quiz_title'])){echo $_POST['quiz_title'];}?>">
				</div>
				<div class="form-group">
					<label for="direction">Extra Quiz Directions</label>
					<input type="text" class="form-control" name="direction" placeholder="<?php if (isset($_POST['direction'])){echo $_POST['direction'];}?>">
				</div>
				
				<input type="hidden" name="quiz_submitted" value="1">
				<button type="submit" name="send_quiz" formmethod="post" class="btn btn-vquiz extra-v-margin">Save Quiz</button>
				</form>
						<!--E1  upload image function -->
				<form enctype="multipart/form-data" method="post">
					Upload Image: <br />
					<input name="upfile" type="file">
					<input type="hidden" name="image_submitted" value="2">
					<input type="submit" name="send" value="Upload" class="btn btn-vquiz extra-v-margin">
					<p>Permitted file types for Upload: .jpg, .jpeg, .png, .pjpeg, .gif, .bmp, .x-png, .swf</p> 
				</form>
		<!--E2  upload image function -->
	</div>
</div>
<div class="row">
	<div class="col-md-7">
				
			<!--Z1  create a question in accordion -->
<div class="panel-group" id="accordion" >
	<form>
    <?php
		//default to one panel open
    	//when button is clicked, make another question with a new question_id of plus one

		?> 


		<button type="button" class="btn btn-primary" id="addnew" onClick="addQuestion('question_block');">Add Question</button>

			<div class="panel panel-default">
				<?php //this panel-body controls the questions so addend this to stuff?>				
					<div class="panel-body">
			    		<form>
			    			<input type="hidden" name="quiz_id" value="<?php //this should query the database and get the quiz that it belongs to but for now?>1">   	
						    <div class="form-group">
					        	<label for="question_number">Question Number</label>
							    <input type="text" class="form-control" name="question_number" placeholder="<?php if (isset($_POST['question_number'])){echo $_POST['question_number'];}?>"> 
							</div>
						    <div class="form-group">
					        	<label for="question_text">Question</label>
							    <input type="text" class="form-control" name="question_text" placeholder="<?php if (isset($_POST['question_text'])){echo $_POST['question_text'];}?>"> 
							</div>
							<div class="form-group">
							    <label for="answer_key">Answer Key</label>
							    <textarea class="form-control" name="answer_key" rows="3" placeholder="<?php if (isset($_POST['answer_key'])){echo $_POST['answer_key'];}?>"></textarea>
							</div>
							<div class="form-group">
							    <label for="image_list">Image</label>

							  	<select class="form-control" name="image_list" id="select">								
									<?php 
										$q1 = "SELECT * FROM image";
										$r1 = mysqli_query($dbc, $q1);

										while ($Image_list = mysqli_fetch_assoc($r1)) {
											$Image_list = data_image_list($dbc, $Image_list['image']);
										?>
										<option value="<?php echo $Image_list['image_list'];?>"><?php echo $Image_list['image_list'];?></option>

									<?php } ?>
								</select>
								<input type="hidden" name="question_submitted" value="1">
								<button type="submit" class="btn btn-vquiz extra-v-margin saver" formmethod="post">Save Question</button>
							</div>
													
						</form>
			      	</div>

				


			</div>
					
	<?php // } ?>
	</form>
</div>

<!--Z2  create a question in accordion -->

	</div>

</div>
<script>
$('.collapse:first').addClass("in");
$('.in').click(function(){
	removeClass("in");
});

// when .saver is clicked, add another panel-body and everything in it to the container
// $('.saver').click(function(){
// 	$().append()
// });
// }
</script>

<?php if($debug == 1) { include('widgets/debug.php'); } ?>

<?php include('theme_1/footer.php'); ?>

