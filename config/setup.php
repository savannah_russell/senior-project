<!-- 
name: setup
author: Paul & Savannah
function: Preload the necessary file.
 
modification log:  
-->

<?php 

session_start();

include('css/css.php'); 
include('js/js.php'); 
include('config/connection.php'); 
include('functions/database-related.php');

#User set-up
$user = get_user($dbc, $_SESSION['username']);

?>