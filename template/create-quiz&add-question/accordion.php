<?php 
	include('css/create-quiz/accordion-css.php'); 
	include('functions/create-quiz&add-question/postback.php');
	include('config/connection.php');
	include('functions/create-quiz&add-question/upload-file.php');
	include('js/create-quiz&add-question/tutorial.php');
?>
<?php
	$user_id = $user['user_id'];
	$q2 = "SELECT * FROM quiz WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
	$r2 = mysqli_query($dbc, $q2);
	$quizInfo = mysqli_fetch_row($r2);
?>
<h1>Create Quiz/Add Question: <?php echo $quizInfo[2]; ?></h1>
</br></br>



<form>
	<button type="submit" name="add_questions" formmethod="post" class="btn btn-danger" id="create-question-window"><i class="fa fa-plus"></i>Add Questions</button>
	<label style="display:hidden;float:right" id="label2" data-placement="top" data-content="Click it again to turn off tutorials." for="tutorial1"></label>
	<button type="button" class="btn btn-success" style="float:right" id="tutorial2">Tutorials</button>
</br>

<div class="panel-group" id="accordion" >
<!-- Quiz information -->
	<div class="panel panel-default">
		<div class="panel-heading">
	      	<h4 class="panel-title">
	      		<a data-toggle="collapse" data-parent="#accordion" href="#999" >
          			Quiz information
        		</a>
	      	</h4>
	    </div>
	    <div name="999" id="999" class="panel-collapse collapse">
	    	<div class="panel-body">
	    		<form>
		    		<div class="row">
			    		<div class="col-md-1"></div>
			    		<div class="col-md-5">
							<label for="question">Quiz name</label></br>
					    	<input type="text" class="form-control" name="quiz_name" value="<?php echo $quizInfo[2]; ?>">
						</div>
			    		<div class="col-md-5">
		 					<label for="answer">Quiz date</label></br>
					    	<input type="text" class="form-control" name="quiz_date" value="<?php echo $quizInfo[3]; ?>">
						</div>
						<div class="col-md-1"></div>
					</div>
					<div class="row">
						<div class="col-md-1"></div>
					    <div class="col-md-5">
					    	<label for="answer">Total point</label></br>
					    	<input type="text" id="total_point" data-placement="top" data-content="Total points of this quiz will be caculated automatically." class="form-control" name="total_grade" value="<?php echo $quizInfo[4]; ?>" readonly>
					    </div>
					    <div class="col-md-5">
					    	<label for="answer">Percent of final point</label></br>
					    	<input type="text" id="percent_of_final" data-placement="top" data-content="Type in 5 means 5%." class="form-control" name="percent_of_final" value="<?php echo $quizInfo[5]; ?>">
					    </div>
					   	<div class="col-md-1"></div>
				   	</div>
				   	<div class="centerize">
						<button type="submit" class="btn btn-success" name="save_quiz_info" formmethod="post">save</button>
	    			</div>
	    		</form>
	    	</div>
	    </div>
	</div>
<!-- Quiz information -->


    <?php
    	$user_id = $user['user_id'];

		$q = "SELECT * FROM question WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
		$r = mysqli_query($dbc, $q);

		while($question_list = mysqli_fetch_assoc($r)) { ?>  
			<div class="panel panel-default"> <!-- class="panel||accordion ..." see what change-->
    			<div class="panel-heading">
	      			<h4 class="panel-title">
		        		<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $question_list['question_id'] ?>">
		          			<?php echo $question_list['question_number'].".".substr($question_list['question'],0,20); ?>
		        		</a>
	      			</h4>
    			</div>
			    <div name="<?php echo $question_list['question_id'] ?>" id="<?php echo $question_list['question_id'] ?>" class="panel-collapse collapse"> <!-- style="display: none;" -->
			    	
					<div class="panel-body">
						<form enctype="multipart/form-data" method="post"> 
							Upload Image: 
							<input name="upfile" type="file" style="display:inline">	<!-- how to put this in the bottom form??? -->
							<input type="submit" id="upload_image" data-toggle="popover" data-content="You can only upload jpg,jpeg,png,pjpeg,gif,bmp,x-png." name="send_image" value="upload" style="width:30;border:1 solid #9a9999; font-size:9pt; background-color:#ffffff" size="17">
						</form>
			    		<form>

						    <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $question_list['question_id']; ?>">
				        	<input type="hidden" name="question_number" readonly="readonly" value="<?php echo $question_list['question_number']; ?>">
				        	<label for="image_list">Image</label>
				        	<select class="form-control" id="image_list" name="image_list" style="display:inline">	 <!-- select image for the question -->							
								
								<?php
									$q1 = "SELECT * FROM image WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";   // show all the images of this quiz in the accordion
									$r1 = mysqli_query($dbc, $q1); ?>
									
									<option value="no image" <?php if('no image' == $question_list['image']) echo "selected=\"selected\""; ?>>no image</option>  <!-- what if the question does not include an image? -->

								<?php	
									while ($Image_list = mysqli_fetch_assoc($r1)) { 
										//$Image_list = select_my_list($dbc, 'image', 'image', $Image_list['image']);
									?>
									<option value="<?php echo $Image_list['image']?>" <?php if($Image_list['image'] == $question_list['image']) echo "selected=\"selected\""; ?>><?php echo $Image_list['image']?></option>

								<?php } ?>
							</select>

							<label for="Point" style="display:inline">Point</label>
							<input name="point" class="form-control" style="display:inline;width:10%" value="<?php echo $question_list['q_point'];?>"></br>
				        	
				        	<label for="question">Question</label></br>
						    <textarea id="question" name="question"><?php echo $question_list['question'];?></textarea>    <!-- question -->
						    
						    <label for="answer">Answer</label> </br>
						    <textarea type="text" id="answer" name="answer"><?php echo $question_list['answer'];?></textarea>
		
							<button type="submit" name="save_question" formmethod="post">save</button>
							<button type="submit" name="delete_question" formmethod="post">delete</button>
						</form>
			      	</div>
			    </div>
			</div>		
	<?php } ?>
</div>
</form>