<?php 
#Start session
session_start();
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}

 include('theme_1/header.php'); 
 ?>

<h1>Results</h1>


<?php 
						$q = "SELECT * FROM results WHERE quiz_id = 4";
						$r = mysqli_query($dbc, $q);
						
						
						while ($results_list = mysqli_fetch_assoc($r)) {
							$results_list = data_results($dbc, $results_list['quiz_id']); 

							
							

							?>

							<div class="bargraph">
								<ul class="bars">
									<li class="bar1"><?php echo $results_list['q1_correct'];?></li>
									<li class="bar2"><?php echo $results_list['q2_correct'];?></li>
									<li class="bar3"><?php echo $results_list['q3_correct'];?></li>
									<li class="bar4"><?php echo $results_list['q4_correct'];?></li>
								</ul>
								<ul class="ylabel">
									
									<li>50</li>
									<li>40</li>
									<li>30</li>
									<li>20</li>
									<li>10</li>
									<li>5</li>
									<li class="units">Percent</li>
								</ul>
								<ul class="graphlabel">
									<li>Results for Quiz 4</li>

								</ul>

							</div>

							



							<?php }		

							?>


<?php include('theme_1/footer.php'); ?>