<style>

.carousel-control.left, .carousel-control.right{
	background:none;
}
.carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-prev, .carousel-control .icon-next{
	color: #2d6ca2;
}

.carousel-caption {
    padding-bottom: 5px;
    width: 80%;
    left: 10%;

}

.carousel-caption img {
    margin-left: 10px;
    margin-right: 40px;
    padding-bottom: 0px;
    float: left;
    width: 46%;
}

#background-image {
	margin-left: auto;
	margin-right: auto;
    width: auto;
    height: 350px;
}

textarea {
    resize: none;
    padding: 1px;
    border: 0px;
    color: rgb(0,0,0);
    width: 45%;
    display: block;
    float: right;
}

.carousel-control {
    width: 10%;
}

.carousel-indicators {
    bottom: 0px;
    margin-bottom: 0px;
}

#student-answer{
    width: 45%;
    padding-left: 2px;
    border: 1px solid #ccc;
    border-radius: 4px;
}

.grade-box {
    text-align:center;
}
</style>