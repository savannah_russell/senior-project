<!DOCTYPE html>

<html>
	<head>
		<?php
			include('config/setup.php');
			include('template/check-user.php');
		?>
	</head>
	<body>
		<div id="wrap">
			<?php include('template/navigation.php'); ?>

			<div class="container">
				<h1>Create Quiz/Add Question</h1>
				</br>
				
				<?php include('template/create-quiz&add-question/select-quiz-button.php'); ?> 
			</div>
		</div>

		<?php include('template/footer.php'); ?>
	</body>
</html>
