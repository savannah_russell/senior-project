<!DOCTYPE html>
<html>
	<head>
		<?php include('config/js.php'); ?>
		<?php include('config/css.php'); ?>
		<?php include('popup-window-fileUpload.php'); ?>
		<style>

   .carousel-caption img {
      margin-left: auto;
      margin-right: auto;
      width: 100%;
      height: absolute;
    }
			#test{
				margin-left: auto;
    		margin-right: auto;
        width: auto;
        height: 680px;
			}
      #test2{
        /*margin-left: auto;
        margin-right: auto;*/
      }

			#texttest{
				color: black;

			}
		</style>

	</head>
	<body>
     <div id="wrap">
  <header>
      <nav class="navbar navbar-default">        
        <ul class="nav navbar-nav">
          <?php //<?php nav_main($dbc, $pageid); ?>
          <li><a href="#">Dashboard</a></li>
          <li><a href="create-quiz.php">Create Quiz</a></li>
          <li><a href="take-quiz.php">Take Quiz</a></li>
          <li><a href="#">Grade Quizzes</a></li>
          <li><a href="#">Results</a></li>
        </ul>
        <div class="user-buttons btn-group">
          
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i> 
            Savannah Russell
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
             <ul class="dropdown-menu" role="menu">
              <!-- <li><button id="btn-debug" class="btn btn-primary navbar-btn"><i class="fa fa-bug"></i> Debug</button></li> -->
              <li><a href="logout.php"><button class="btn btn-primary navbar-btn"><i class="fa fa-sign-out"></i> Logout</button></a></li>
            </ul>
      </div>
      </nav>
    </header>
    <h1>Quiz Title</h1>                                                             <!--stop the auto cycling -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
      <!-- Indicators -->
      <ol class="carousel-indicators">  
        <?php 
          $q = "SELECT * FROM question";
          $r = mysqli_query($dbc, $q);
          $i = 0;

          while ($my_list = mysqli_fetch_assoc($r)) {
            $question_list = select_my_list($dbc, 'question', 'question', $my_list['question']);
          ?>
          <li data-target="#myCarousel" style="background-color:black;" data-slide-to="<?php echo "$i"; $i++; ?>" class="<?php if($i == 1){echo "active"; } ?>"></li>

        <?php } ?>
      </ol>
      <div class="carousel-inner">

        			  <?php 
        					$q1 = "SELECT * FROM question";
        					$r1 = mysqli_query($dbc, $q1);
                  $i = 1;
        					while ($my_list = mysqli_fetch_assoc($r1)) {

        						$my_question_list = select_my_list($dbc, 'question', 'question', $my_list['question']);
                    $my_answer_list = select_my_list($dbc, 'question', 'answer', $my_list['answer']);
                    $my_image_list = select_my_list($dbc, 'question', 'image', $my_list['image']);
        					?>
                  <div class="item<?php if($i==1){echo " active"; $i++;} ?>">
                    <img src="upload/untitled.jpg" alt="First slide" id="test" >

                    <div class="container">
                      <div class="carousel-caption" id="test2">
                       
                          <img src="upload/<?php echo $my_image_list['my_index']; ?>">
                          <h1><?php echo $my_question_list['my_index']?></h1>

                          <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $my_list['question_id'] ?>"> <!-- find the ID of the question -->

                          <textarea rows="4" cols="80" id="texttest" name="student_answer">
        					          <?php echo $my_answer_list['my_index']?>
                          </textarea>
                          <button type="submit" name="save_answer" style="color:black" formmethod="post">save</button>
                        
                      </div>
                    </div>
                  </div>
        			  <?php } ?>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    <footer><p>Developed by Pierce Software for Drury University, 2014</p></footer>
  </div>
	</body>
</html>