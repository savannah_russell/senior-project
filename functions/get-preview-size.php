
<?php  
function get_preview_size($image, $quiz_id, $edge, $page){

	$image_size = getimagesize("images/$quiz_id/".$image);
	$previewsize = 1;
	$wanted_height = 1;
	
	if($page == "T") { //take quiz page
		while($image_size[1] * $previewsize < 330){  // image too small
		  $previewsize = $previewsize * 121 / 120;            
		}
		while($image_size[1] * $previewsize > 348 || $image_size[0] * $previewsize > 472){  // image too big
		  $previewsize = $previewsize * 120 / 121;            
		}
	}
	elseif ($page == "G") { //grade quiz page
		while($image_size[1] * $previewsize > 300 || $image_size[0] * $previewsize > 445){  // image too big
		  $previewsize = $previewsize * 120 / 121;            
		}
		while($image_size[1] * $previewsize < 285 && $image_size[0] * $previewsize < 430){	// image too small
		  $previewsize = $previewsize * 121 / 120;            
		}
	}
	
	if($edge == "width")
		return $image_size[0] * $previewsize;
	elseif ($edge == "height") 
		return $image_size[1] * $previewsize;
}
?>