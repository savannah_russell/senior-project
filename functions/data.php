<?php 

function data_setting_value($dbc, $id){
	$q = "SELECT * FROM settings WHERE id = '$id'";
	$r = mysqli_query($dbc, $q);

	$data = mysqli_fetch_assoc($r);

	return $data['value'];
}

function data_page($dbc, $pageid){ //Grab pages
	$query = "SELECT * FROM pages WHERE page_id = $pageid";
	$result = mysqli_query($dbc, $query);
	$data = mysqli_fetch_assoc($result);
	$data['body_nohtml'] = strip_tags($data['page_content']);

	if($data['page_content'] == $data['body_nohtml']) { //Place tags around content if there were no tags inside it

		$data['body_formatted'] = '<p>'.$data['page_content'].'</p>';
	}
	else {
		$data['body_formatted'] = $data['page_content'];
	}
	
	return $data;
}

function data_user($dbc, $id){

	// if(=){
	// 	$cond = "WHERE email = '$id'";
	// }
	// else{
	// 	$cond = "WHERE username = '$id'"; 
	// }


	$q = "SELECT * FROM user WHERE email = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	$data['fullname'] = $data['first_name'].' '.$data['last_name'];
	// $data['fullname_reverse'] = $data['last_name'].', '.$data['first_name'];
	return $data;
}

function data_class_list($dbc, $id){
	$q = "SELECT DISTINCT class_code FROM quizzes WHERE class_code = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	$data['class_list'] = $data['class_code'];
	return $data;
}

function data_question_list($dbc, $id){
	$q ="SELECT * FROM quizzes WHERE quiz_id = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	$data['question_list'] = $data['quiz_id'];
	return $data;
}

function data_results($dbc, $id){
	$q = "SELECT * FROM results WHERE quiz_id = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	$data['results_list'] = $data['quiz_id'];
	return $data;
}

;?>