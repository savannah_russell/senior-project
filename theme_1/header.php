<?php 
	include('setup.php');
	
  session_start();

?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page['page_title']. ' | ' .$site_title; ?></title>
   	<?php include('spare-css.php');?>
	<?php include('js.php');?>

  </head>
  <body>
  	<div id="wrap">
  	<header>
	  	<nav class="navbar navbar-default">
	  		<ul class="nav navbar-nav">
          <?php nav_main($dbc, $pageid); ?>
	  			
	  		</ul>
        <div class="user-buttons">
          <!-- <button id="btn-debug" class="btn btn-primary"><i class="fa fa-bug"></i></button> -->
          <a href="./admin/login.php"><button id="login" class="btn btn-primary">Log In</button></a>
          <a href="./register.php"><button id="register" class="btn btn-primary">Register</button></a>
      </div>
	  	</nav>
  	</header>
  	<div class="container">