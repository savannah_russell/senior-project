<style>

.carousel-control.left, .carousel-control.right{
	background:none;
}
.carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-prev, .carousel-control .icon-next{
	color: #2d6ca2;
}

.carousel-caption {
    padding-bottom: 5px;
    width: 80%;
    left: 10%;

}

.carousel-caption img {

    margin-left: auto;
    margin-right: auto;
    padding-bottom: 0px;
}

#background-image {
	margin-left: auto;
	margin-right: auto;
    width: auto;
    height: 535px;
}

textarea {
    resize: none;
    padding: 0px;
    border: 0px;
    color: rgb(0,0,0);
    width: 100%;
}

.carousel-control {
    width: 10%;
}

.carousel-indicators {
    bottom: 0px;
    margin-bottom: 0px;
}

#footer {
    margin-bottom: 0px;
}

#wrap {
    padding: 0 0 0;
}
	

</style>