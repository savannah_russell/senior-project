<?php
  $uptypes=array('image/jpg',
  'image/jpeg',
  'image/png',
  'image/pjpeg',
  'image/gif',
  'image/bmp',
  'image/x-png'); 
  $max_file_size=5000000;   //上传文件大小限制, 单位BYTE
  $imgpreview=1;   //是否生成预览图(1为生成,其他为不生成);
  $imgpreviewsize=1/2;  //缩略图比例
  $uploadOk = 1;

  $user_id = $user['user_id'];
  $q1 = "SELECT temporal_data FROM user WHERE user_id = '$user_id'";
  $r1 = mysqli_query($dbc, $q1);
  $quiz_id = mysqli_fetch_row($r1);

  if(isset($_POST['send_image']))   // when user upload an image
  {
    $destination_folder="images/$quiz_id[0]/";//上传文件路径
    if (!is_uploaded_file($_FILES["upfile"]["tmp_name"]))
    //是否存在文件
    { 
      echo "<font color='red'>file does not existed!</font></br>";
      $uploadOk = 0;
    }

    $file = $_FILES["upfile"];
    if($max_file_size < $file["size"])
    //检查文件大小
    {
      echo "<font color='red'>size of the file is too big!</font>";
      $uploadOk = 0;
    }

    if(!in_array($file["type"], $uptypes))
    //检查文件类型
    {
      echo "<font color='red'>you can only upload image or flash!</font>";
      $uploadOk = 0;
    }

    if(!file_exists($destination_folder))
      mkdir($destination_folder);

    $filename=$file["tmp_name"];
    $pinfo=pathinfo($file["name"]);
    $ftype=$pinfo["extension"];
    $destination = $destination_folder.$file['name'];

    if (file_exists($destination) && $overwrite != true)
    {
       echo "<font color='red'>file already existed!</font>";
       $uploadOk = 0;
    }

    if(!move_uploaded_file ($filename, $destination))
    {
      echo "<font color='red'>file moving error!</a>";
      $uploadOk = 0;
    }

    if($uploadOk == 1) 
    {
      $pinfo=pathinfo($destination);
      $fname=$pinfo["basename"];
      echo " <font color=red>Upload successfully</font>";

      //------ insert image name in database ------
      $filename = "".$file['name']."";
      mysqli_query($dbc,"INSERT INTO image (image,quiz_id) VALUES ('$filename',$quiz_id[0])");
      //-------------------------------------------
    }
    
  }


?>