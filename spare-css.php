<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<!--Font Awesome-->
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<!-- Bootstrap -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

<!--custom css until I can finangle with it-->
<style>
	#wrap {
		width: 1000px;
		margin-left: auto;
		margin-right: auto;
	}
	.container {
		width: 100%;
	}
	.user-buttons {
		float: right;
		margin-right: 20px;
		margin-top: 7px;
	}
	.btn-vquiz {
		background-color: #1abc9c;
		color: #ffffff;
	}
	.btn-vquiz:hover {
		background-color: #16a085;
		color: #ffffff;
	}
	.panel-info>.panel-heading{
		color: #ffffff;
		background-color: #1abc9c;
		background-image: none;
	}
	.navbar-default{
		color: #ffffff;
		background-image: none;
		background-color: #1abc9c;
	}
	.navbar-default .navbar-nav>li>a{
		color: #ffffff;
	}
	.navbar-default .navbar-nav>li>a:hover{
		color: #ffffff;
	}
</style>

