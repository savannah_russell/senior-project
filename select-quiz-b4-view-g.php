<!-- 
name: select quiz
author: Paul
function: select a quiz first.
 
modification log:  
-->
<!DOCTYPE html>

<html>
	<head>
		<?php
			include('config/setup.php');
			include('template/check-user.php');
		?>
	</head>
	<body>
		<div id="wrap">
			<?php include('template/navigation.php'); ?>

			<div class="container">
				<h1>View Grade</h1></br>
				
				<?php include('template/view-grade/select-quiz-button.php'); ?>

			</div>
			
		</div>

		<?php include('template/footer.php'); ?>
	</body>
</html>