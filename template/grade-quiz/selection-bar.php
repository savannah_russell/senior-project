<!-- 
name: selection bar
author: Paul 
function: Set up the selection bar in grade quiz page.
 
modification log:

-->
<?php 
	include('config/connection.php');
	include('functions/grade-quiz/postback.php');
	include('js/grade-quiz/grade-quiz-js.php');
	include('js/grade-quiz/tutorial.php');
?>
<?php
	$user_id = $user['user_id'];
	$q2 = "SELECT * FROM quiz WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
	$r2 = mysqli_query($dbc, $q2);
	$quizInfo = mysqli_fetch_row($r2);
?>
<h1 style="display:inline">Grade Quiz: <?php echo $quizInfo[2]; ?></h1>
<label style="display:hidden;float:right" id="label2" data-placement="right" data-content="Click it again to turn off tutorials." for="tutorial2"></label>
<button type="button" class="btn btn-success" style="float:right" id="tutorial2">Tutorials</button>
</br>

<div class="row">
	<form name="selection-bar">
		
    	<div class="col-md-2">
    		
		</div>
    	<div class="col-md-3">
    		<select class="form-control" name="rotate_by" id="rotate-bar" onchange="selectBar()">
				<option value="Question Number">rotate by question number</option>
				<option value="Student Name">rotate by student name</option>
			</select>
    	</div>		
    	<div class="col-md-3">
    		<select class="form-control" id="qn-bar" name="question_number" style="display:none;">
				<option value="Question Number">Question Number</option>
				<?php
					$user_id = $user['user_id'];
					$q1 = "SELECT temporal_data FROM user WHERE user_id = '$user_id'";
					$r1 = mysqli_query($dbc, $q1);
					$quiz_id = mysqli_fetch_row($r1);

					$q = "SELECT * FROM question WHERE quiz_id = $quiz_id[0]";
					$r = mysqli_query($dbc, $q);

					while ($Question_list = mysqli_fetch_assoc($r)) { ?>

					<option value="<?php echo $Question_list['question_id']; ?>"><?php echo $Question_list['question_number']; ?></option>
					
				<?php } ?>
			</select>
		</div>
    	<div class="col-md-3">
    		<select class="form-control" id="sn-bar" name="student_name"  style="display:inline;">
				<option value="student name">Student Name</option>
				<?php
					$user_id = $user['user_id'];
					$q = "SELECT * FROM user_course AS uc LEFT JOIN user AS u on uc.user_id = u.user_id WHERE course_id = (SELECT td3 FROM user WHERE user_id = $user_id)";
					$r = mysqli_query($dbc, $q);

					while ($Student_list = mysqli_fetch_assoc($r)) { ?>

					<option value="<?php echo $Student_list['user_id']?>"><?php echo $Student_list['first_name']." ".$Student_list['last_name']?></option>
					
				<?php } ?>
			</select>
		</div>
    	<div class="col-md-1">
    		<button type="submit" data-placement="bottom" data-content="Select the style you want to grade first." id="how_to_grade" formmethod="post" class="btn btn-danger" name="go" style="color:black"><i class="fa fa-play"></i></button>
    	</div>
	</form>
</div>