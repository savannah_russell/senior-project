<!-- 
name: database functions
author: Paul & Savannah
function: Every functions in this file are related to the database.
 
modification log:  
-->

<?php
function get_user($dbc, $id){

	$q = "SELECT * FROM user WHERE email = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	$data['fullname'] = $data['first_name'].' '.$data['last_name'];
	// $data['fullname_reverse'] = $data['last_name'].', '.$data['first_name'];
	return $data;
}

function select_my_data($dbc, $table, $index, $id){       //get the specific data I want 
	$q = "SELECT $index FROM $table WHERE question_id = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	$data['my_choice'] = $data[$index];

	$result = mysqli_real_escape_string($dbc, $data['my_choice']);

	return $result;
}

function select_my_list($dbc, $table, $index, $id){      //get the specific list of data I want
  $q = "SELECT $index FROM $table WHERE $index = '$id'";
  $r = mysqli_query($dbc, $q);
  $data = mysqli_fetch_assoc($r);
  $data['my_index'] = $data[$index];
  return $data;
}

?>