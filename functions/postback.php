<?php

if(isset($_POST['save_question'])) // professor save the question and answer in create quiz page
{
	
	$questionID = mysqli_real_escape_string($dbc, $_POST['questionID']);
	$question = mysqli_real_escape_string($dbc, $_POST['question']);
	$questionNum = mysqli_real_escape_string($dbc, $_POST['question_number']);
	$answer = mysqli_real_escape_string($dbc, $_POST['answer']);
	$image = mysqli_real_escape_string($dbc, $_POST['image_list']);

	mysqli_query($dbc,"UPDATE question SET question='$question' WHERE question_id='$questionID'");
	mysqli_query($dbc,"UPDATE question SET question_number='$questionNum' WHERE question_id='$questionID'");
	mysqli_query($dbc,"UPDATE question SET answer='$answer' WHERE question_id='$questionID'");
	mysqli_query($dbc,"UPDATE question SET image='$image' WHERE question_id='$questionID'");	
}

if(isset($_POST['save_quiz_info']))
{
	$user_id = $user['user_id'];
	$q = "SELECT * FROM question WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
	$r = mysqli_query($dbc, $q);
	$quiz_id = mysqli_fetch_row($r); // $quiz_id is an array

	$quiz_name = mysqli_real_escape_string($dbc, $_POST['quiz_name']);
	$quiz_date = mysqli_real_escape_string($dbc, $_POST['quiz_date']);

	mysqli_query($dbc,"UPDATE quiz SET quiz_name='$quiz_name' WHERE quiz_id = $quiz_id[0]");
	mysqli_query($dbc,"UPDATE quiz SET quiz_date='$quiz_date' WHERE quiz_id = $quiz_id[0]");
}

if(isset($_POST['delete_question'])) 
{
	$q = "SELECT * FROM question WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
	$r = mysqli_query($dbc, $q);
	$quiz_id = mysqli_fetch_row($r); // $quiz_id is an array
	$questionID = mysqli_real_escape_string($dbc, $_POST['questionID']);
	$question_number = mysqli_real_escape_string($dbc, $_POST['question_number']);

	mysqli_query($dbc,"DELETE FROM question WHERE question_id='$questionID'");
	mysqli_query($dbc,"UPDATE question SET question_number = question_number - 1 WHERE quiz_id = $quiz_id[0] AND question_number > $question_number");

	//mysqli_query($dbc,"SET @count = 0");   reset question_id
	//mysqli_query($dbc,"UPDATE question SET question_id = @count:= @count + 1");

}

if(isset($_POST['save_answer'])) //student save the answer in take quiz page
{
	$student_answer = mysqli_real_escape_string($dbc, $_POST['student_answer']);
	$questionID = mysqli_real_escape_string($dbc, $_POST['questionID']);

	$fn = $user['first_name'];
	$ln = $user['last_name'];
	$user_id = $user['user_id'];

	$q = "SELECT * FROM question WHERE quiz_id = (SELECT temporal_data FROM user WHERE user_id = $user_id)";  //16:33 2014 9 1
	$r = mysqli_query($dbc, $q);
	$quiz_id = mysqli_fetch_row($r); // $quiz_id is an array

	$q1 = "SELECT * FROM user_question WHERE user_id = $user_id AND question_id = '$questionID'";
   	$r1 = mysqli_query($dbc, $q1);

 
    if (mysqli_num_rows($r1) == 0) {
		mysqli_query($dbc,"INSERT INTO user_question (user_id,quiz_id,question_id,student_answer) VALUES ($user_id,$quiz_id[0],$questionID,'$student_answer')");
	}
	else {
		mysqli_query($dbc,"UPDATE user_question SET student_answer='$student_answer' WHERE question_id = '$questionID' AND user_id = '$user_id'");
	}
	
}


if(isset($_POST['add_course']))
{

	$user_id = $user['user_id'];

	$uniq_code = rand(0,1000000);  //generate unique course code for student to sign up

	$q = "SELECT course_code FROM course WHERE course_code = $uniq_code";
 	$r = mysqli_query($dbc, $q);

	while(mysqli_num_rows($r) != 0) {
	  	$uniq_code = rand(0,1000000);
	}

	mysqli_query($dbc,"INSERT INTO course (course_number,instructor_id,section_number,course_name,department,year,course_code) VALUES (0, $user_id, 0,'undefine','undefine','undefine',$uniq_code)");

}


if(isset($_POST['save_course']))
{

	$course_id = mysqli_real_escape_string($dbc, $_POST['course_id']);
	$course_number = mysqli_real_escape_string($dbc, $_POST['course_number']);
	$section_number = mysqli_real_escape_string($dbc, $_POST['section_number']);
	$course_name = mysqli_real_escape_string($dbc, $_POST['course_name']);
	$department = mysqli_real_escape_string($dbc, $_POST['department']);
	$year = mysqli_real_escape_string($dbc, $_POST['year']);
	$course_code = mysqli_real_escape_string($dbc, $_POST['course_code']);

	mysqli_query($dbc,"UPDATE course SET course_number='$course_number' WHERE course_id='$course_id'");
	mysqli_query($dbc,"UPDATE course SET section_number='$section_number' WHERE course_id='$course_id'");
	mysqli_query($dbc,"UPDATE course SET course_name='$course_name' WHERE course_id='$course_id'");
	mysqli_query($dbc,"UPDATE course SET department='$department' WHERE course_id='$course_id'");
	mysqli_query($dbc,"UPDATE course SET year='$year' WHERE course_id='$course_id'");
	mysqli_query($dbc,"UPDATE course SET course_code='$course_code' WHERE course_id='$course_id'");

}

if(isset($_POST['go_back']))
{
	header('Location: ../../create-course.php');
}


if(isset($_POST['sign_up_confirm']))
{

	$firstname = mysqli_real_escape_string($dbc, $_POST['first_name']);
	$middlename = mysqli_real_escape_string($dbc, $_POST['middle_name']);
	$lastname = mysqli_real_escape_string($dbc, $_POST['last_name']);
	$email = mysqli_real_escape_string($dbc, $_POST['email']);
	$password = mysqli_real_escape_string($dbc, $_POST['password']);
	$course_code = mysqli_real_escape_string($dbc, $_POST['code']);

	$deC = crypt($password, "drury");
	$q = "SELECT * FROM user WHERE email = '$email' AND first_name = '$firstname' AND middle_name = '$middlename' AND last_name = '$lastname'";
 	$r = mysqli_query($dbc, $q);

 	if (mysqli_num_rows($r) == 0) {
	  	mysqli_query($dbc,"INSERT INTO user (first_name, middle_name, last_name, email, password, status) VALUES ('$firstname', '$middlename', '$lastname', '$email', '$deC', 'student')");
		
		header('Location: login.php');
	} else {
		echo "User already existed!";
	}
}

if(isset($_POST['sign_up_course']))
{
	$course_code = mysqli_real_escape_string($dbc, $_POST['course_code']);
	if(is_numeric($course_code)){
		$user_id = $user['user_id'];
		$q1 = "SELECT course_id FROM course WHERE course_code = $course_code";
		$r1 = mysqli_query($dbc, $q1);
		$course_id = mysqli_fetch_row($r1);
		if(mysqli_num_rows($r1) == 0){

			echo "<font color='red'>Course code does not existed!</font>";

		} else {

			$q2 = "SELECT * FROM user_course WHERE user_id = $user_id AND course_id = $course_id[0]";
			$r2 = mysqli_query($dbc, $q2);
			if (mysqli_num_rows($r2) == 0) {
				mysqli_query($dbc,"INSERT INTO user_course (user_id, course_id) VALUES ('$user_id','$course_id[0]')");
				echo "<font color='red'>Successful!</font>";
			} else {
				echo "<font color='red'>You've already sign up this course!</font>";
			}
		}
	} else {
		echo "<font color='red'>Course code does not existed!</font>";
	}

}
?>