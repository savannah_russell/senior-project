<!-- 
name: footer
author: Paul & Savannah
function: Set up the footer.
 
modification log:  
-->

<div id="footer">
	<div class="container">
		<p class="text-muted">Developed by Pierce Software for Drury University, 2014</p>
	</div>
</div>