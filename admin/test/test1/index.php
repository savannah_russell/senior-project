<!DOCTYPE html>
<html>
  <head>

    <?php include('config/js.php'); ?>
    <?php include('config/css.php'); ?>
    <?php include('popup-window-fileUpload.php'); ?>
    <?php include('select-data-from-mysql.php'); ?>

    <style>
        label, input { display:block; }
      </style>
<!--C1 pops up a create question window -->
    <script>
        $(function() {
 
          $( "#dialog-form-create" ).dialog({
              autoOpen: false,
          });
 
          $( "#create-question-window" ).button().click(function() {
              $( "#dialog-form-create" ).dialog( "open" );
            });
       });
      </script>
<!--C2 pops up a create question window -->

    <script>
      $(function() {
        $('.collapse').collapse();
      });
    </script>
<script>
  function test2(a){
    $( "#dialog-form-edit" ).dialog( "open" );

    document.getElementById("question2").setAttribute("value", "my value");
  }
</script>

  </head>

  <body>    
    <h1>Create Quiz</h1>

<!--E1  upload image function -->
    <form enctype="multipart/form-data" method="post">
      Upload Image: <br>
      <input name="upfile" type="file">
      <input type="submit" name="send" value="upload" style="width:30;border:1 solid #9a9999; font-size:9pt; background-color:#ffffff" size="17">
      file type can Upload:jpg|jpeg|png|pjpeg|gif|bmp|x-png|swf <br><br>
    </form>
<!--E2  upload image function -->

<!--Z1  create a question in accordion -->
<div class="panel-group" id="accordion" >
    <?php
    $q = "SELECT * FROM question";
    $r = mysqli_query($dbc, $q);

    while($question_list = mysqli_fetch_assoc($r)) { ?>  
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $question_list['question_id'] ?>">
                    Question <?php echo $question_list['question_id'] ?>
                </a>
              </h4>
          </div>
          <div name="<?php echo $question_list['question_id'] ?>" id="<?php echo $question_list['question_id'] ?>" style="display: none;" class="panel-collapse collapse in">
            <div class="panel-body">
              <form>
                <label for="name">Question_id</label>
                <input type="text" name="questionID" readonly="readonly" value="<?php echo $question_list['question_id'] ?>">
                  <label for="name">Question</label>
                <input type="text" name="question">
                <label for="email">Answer</label>
                <input type="text" name="answer">
                <label for="password">Image</label>

                <select class="form-control" name="image_list" id="select">               
                <?php 
                  $q1 = "SELECT * FROM image";
                  $r1 = mysqli_query($dbc, $q1);

                  while ($Image_list = mysqli_fetch_assoc($r1)) {
                    $Image_list = data_image_list($dbc, $Image_list['image']);
                  ?>
                  <option value="<?php echo $Image_list['image_list']?>"><?php echo $Image_list['image_list']?></option>

                <?php } ?>
              </select>
              <button type="submit" name="save" formmethod="post">save</button>
            </form>
              </div>
          </div>
      </div>    
  <?php } ?>
</div>
<!--Z2  create a question in accordion -->


  <form>
    <button type="submit" name="create" formmethod="post" class="btn btn-default" id="create-question-window"><i class="fa fa-plus"></i></button>
  </form>
  </body>
</html>