<?php
//setup file

//where you put the database
$dbc = mysqli_connect('mcs.drury.edu', 'piercesav', 'password', 'visquizbuild') OR die('Error'.mysqli_connect_error());
// $dbc = mysqli_connect('localhost', 'pierce', 'piercesoftware', 'vqbuild') OR die('Error'.mysqli_connect_error());

#Constants
DEFINE('D_THEME', 'theme_1'); //set base for new themes not based around Bootstrap

#Functions
include('functions/data.php'); //Calls data
include('functions/template.php'); //Calls up navigation
include('functions/paul-functions.php'); //Calls up Paul's functions


#Site Setup
$debug = data_setting_value($dbc, 'debug-status');

$site_title = 'VisQuiz';
$page_title = 'Home';

if(isset($_GET['page_id'])) {
	$pageid = $_GET[page_id]; //Pull up content based on page's id
}
else {
	$pageid = '1'; //Default to home, eventually
}

#Page Set-up
$page = data_page($dbc, $pageid);

#User set-up
$user = data_user($dbc, $_SESSION['username']);



?>