<?php
	$user_id = $user['user_id'];
	$q1 = "SELECT temporal_data FROM user WHERE user_id = '$user_id'";
	$r1 = mysqli_query($dbc, $q1);
	$quiz_id = mysqli_fetch_row($r1);

	if(isset($_POST['edit_quiz']))
	{	
		$quiz_id = mysqli_real_escape_string($dbc, $_POST['quiz_id']);
		mysqli_query($dbc,"UPDATE user SET temporal_data='$quiz_id' WHERE user_id='$user_id'");  //temporal_data store the quiz_id
		header('Location: add-question.php');
	}

	if(isset($_POST['add_quiz']))
	{
		//$course_list = mysqli_real_escape_string($dbc, $_POST['course_list']);
		$q = "SELECT td3 FROM user WHERE user_id = '$user_id'";
		$r = mysqli_query($dbc,$q);
		$course_id = mysqli_fetch_row($r);
		
		mysqli_query($dbc,"INSERT INTO quiz (course_id) VALUES ($course_id[0])");
	}

	if(isset($_POST['add_questions']))  // professor click the create question button in create quiz page.
	{
	//	echo $quiz_id[0];
		$q = "SELECT * FROM question WHERE quiz_id = $quiz_id[0] AND question_number = '1'";
	 	$r = mysqli_query($dbc, $q);
		if (mysqli_num_rows($r) == 0)   // if it is the first question in the quiz, question_num set to 1.
		{	
		  	mysqli_query($dbc,"INSERT INTO question (quiz_id,question_number) VALUES ($quiz_id[0], 1)");
		}
		else
		{
			$q2 = "SELECT MAX(question_number) FROM question WHERE quiz_id = $quiz_id[0]"; // get the highest question number in the quiz.
	 		$r2 = mysqli_query($dbc, $q2);

	 		$highestNum = mysqli_fetch_row($r2);

	 		mysqli_query($dbc,"INSERT INTO question (quiz_id,question_number) VALUES ($quiz_id[0],$highestNum[0]+1)");
		}
	}
	if(isset($_POST['delete_question'])) 
	{

		$questionID = mysqli_real_escape_string($dbc, $_POST['questionID']);
		$question_number = mysqli_real_escape_string($dbc, $_POST['question_number']);
		mysqli_query($dbc,"DELETE FROM question WHERE question_id='$questionID'");
		mysqli_query($dbc,"UPDATE question SET question_number = question_number - 1 WHERE quiz_id = $quiz_id[0] AND question_number > $question_number");

		//mysqli_query($dbc,"SET @count = 0"); //  reset question_id
		//mysqli_query($dbc,"UPDATE question SET question_id = @count:= @count + 1");

	}
	if(isset($_POST['save_question'])) // professor save the question and answer in create quiz page
	{
		
		$questionID = mysqli_real_escape_string($dbc, $_POST['questionID']);
		$question = mysqli_real_escape_string($dbc, $_POST['question']);
		$questionNum = mysqli_real_escape_string($dbc, $_POST['question_number']);
		$answer = mysqli_real_escape_string($dbc, $_POST['answer']);
		$image = mysqli_real_escape_string($dbc, $_POST['image_list']);
		$point = mysqli_real_escape_string($dbc, $_POST['point']);

		mysqli_query($dbc,"UPDATE question SET question='$question' WHERE question_id='$questionID'");
		mysqli_query($dbc,"UPDATE question SET question_number='$questionNum' WHERE question_id='$questionID'");
		mysqli_query($dbc,"UPDATE question SET answer='$answer' WHERE question_id='$questionID'");
		mysqli_query($dbc,"UPDATE question SET image='$image' WHERE question_id='$questionID'");
		mysqli_query($dbc,"UPDATE question SET q_point='$point' WHERE question_id='$questionID'");

		$q = "SELECT SUM(q_point) FROM question WHERE quiz_id = '$quiz_id[0]'";
		$r = mysqli_query($dbc, $q);
		$total_grade = mysqli_fetch_row($r);

		mysqli_query($dbc,"UPDATE quiz SET total_grade = '$total_grade[0]' WHERE quiz_id = '$quiz_id[0]'");
	}

	if(isset($_POST['save_quiz_info']))
	{
		$quiz_name = mysqli_real_escape_string($dbc, $_POST['quiz_name']);
		//$quiz_year = mysqli_real_escape_string($dbc, $_POST['quiz_year']);
		//$quiz_month = mysqli_real_escape_string($dbc, $_POST['quiz_month']);
		$quiz_date = mysqli_real_escape_string($dbc, $_POST['quiz_date']);
		$total_grade = mysqli_real_escape_string($dbc, $_POST['total_grade']);
		$percent_of_final = mysqli_real_escape_string($dbc, $_POST['percent_of_final']);

		//if($quiz_date < 10)
		//	$quiz_date = '0'.$quiz_date;
		//if($quiz_month < 10)
		//	$quiz_month = '0'.$quiz_month;
		//$date = $quiz_month.$quiz_date.$quiz_year;

		mysqli_query($dbc,"UPDATE quiz SET quiz_name='$quiz_name' WHERE quiz_id = $quiz_id[0]");
		mysqli_query($dbc,"UPDATE quiz SET quiz_date='$quiz_date' WHERE quiz_id = $quiz_id[0]");
		mysqli_query($dbc,"UPDATE quiz SET total_grade='$total_grade' WHERE quiz_id = $quiz_id[0]");
		mysqli_query($dbc,"UPDATE quiz SET percent_of_final='$percent_of_final' WHERE quiz_id = $quiz_id[0]");
	}
	
	if(isset($_POST['select_course']))
	{
		$course = mysqli_real_escape_string($dbc, $_POST['course_list']);
		mysqli_query($dbc,"UPDATE user SET td3='$course' WHERE user_id='$user_id'");  //td3 store the course_id
		
		header('Location: select-quiz-b4-create-q.php');
	}
?>