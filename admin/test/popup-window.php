<!DOCTYPE html>
<html>
	<head>

		<?php include('config/js.php'); ?>
		<?php include('config/css.php'); ?>
		<?php include('popup-window-fileUpload.php'); ?>
		<?php include('select-data-from-mysql.php'); ?>

		<style>
    		label, input { display:block; }
  		</style>
<!--C1 pops up a create question window -->
		<script>
  			$(function() {
 
    			$( "#dialog-form-create" ).dialog({
      				autoOpen: false,
    			});
 
    			$( "#create-question-window" ).button().click(function() {
        			$( "#dialog-form-create" ).dialog( "open" );
      			});
 			 });
  		</script>
<!--C2 pops up a create question window -->

<script>
	function test2(a){
		$( "#dialog-form-edit" ).dialog( "open" );

		document.getElementById("question2").setAttribute("value", "my value");
	}
</script>

	</head>

	<body>
	<div id="wrap">
	<header>
	  	<nav class="navbar navbar-default">        
	  		<ul class="nav navbar-nav">
          <?php //<?php nav_main($dbc, $pageid); ?>
	  			<li><a href="index.php">Dashboard</a></li>
	  			<li><a href="createquiz2.php">Create Quiz</a></li>
	  			<li><a href="just-for-test.php">Take Quiz</a></li>
          <li><a href="#">Grade Quizzes</a></li>
          <li><a href="results.php">Results</a></li>
	  		</ul>
        <div class="user-buttons btn-group">
          
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i> 
            Savannah Russell
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
             <ul class="dropdown-menu" role="menu">
              <!-- <li><button id="btn-debug" class="btn btn-primary navbar-btn"><i class="fa fa-bug"></i> Debug</button></li> -->
              <li><a href="logout.php"><button class="btn btn-primary navbar-btn"><i class="fa fa-sign-out"></i> Logout</button></a></li>
            </ul>
      </div>
	  	</nav>
  	</header>

		<h1>Create Quiz</h1>

<!--E1  upload image function -->
		<form enctype="multipart/form-data" method="post">
			Upload Image: <br>
			<input name="upfile" type="file">
			<input type="submit" name="send" value="upload" style="width:30;border:1 solid #9a9999; font-size:9pt; background-color:#ffffff" size="17">
			file type can Upload:jpg|jpeg|png|pjpeg|gif|bmp|x-png|swf <br><br>
		</form>
<!--E2  upload image function -->
		<form>
			<button type="submit" name="create" formmethod="post" class="btn btn-default" id="create-question-window"><i class="fa fa-plus"></i></button>
		</form>
<!--Z1  create a question in accordion -->
<div class="panel-group" id="accordion" >
    <?php
		$q = "SELECT * FROM question";
		$r = mysqli_query($dbc, $q);

		while($question_list = mysqli_fetch_assoc($r)) { ?>  
			<div class="panel panel-default"> <!-- class="panel||accordion ..." see what change-->
    			<div class="panel-heading">
	      			<h4 class="panel-title">
		        		<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $question_list['question_id'] ?>">
		          			Question <?php echo $question_list['question_id'] ?>
		        		</a>
	      			</h4>
    			</div>
			    <div name="<?php echo $question_list['question_id'] ?>" id="<?php echo $question_list['question_id'] ?>" class="panel-collapse collapse"> <!-- style="display: none;" -->
			    	<div class="panel-body">
			    		<form>
				    	<!--	<label for="name">Question_id</label>  -->
						    <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $question_list['question_id'] ?>">
				        	<label for="name">Question</label>
						    <input type="text" name="question" value="<?php echo select_my_data($dbc, 'question', 'question', $question_list['question_id']);?>"> 
						    <label for="email">Answer</label>
						    <input type="text" name="answer" value="<?php echo select_my_data($dbc, 'question', 'answer', $question_list['question_id']);?>" >
						    <label for="password">Image</label>

						  	<select class="form-control" name="image_list" id="select">								
								<?php 
									$q1 = "SELECT * FROM image";
									$r1 = mysqli_query($dbc, $q1);

									while ($Image_list = mysqli_fetch_assoc($r1)) {
										$Image_list = data_image_list($dbc, $Image_list['image']);
									?>
									<option value="<?php echo $Image_list['image_list']?>"><?php echo $Image_list['image_list']?></option>

								<?php } ?>
							</select>
							<button type="submit" name="save" formmethod="post">save</button>
						</form>
			      	</div>
			    </div>
			</div>		
	<?php } ?>
</div>
<!--Z2  create a question in accordion -->
<footer><p>Developed by Pierce Software for Drury University, 2014</p></footer>

</div>


	
	</body>
</html>