<!-- 
name: create course button
author: Paul 
function: Professors click on the button can add a new course.
 
modification log: 
16:46 7/16/2014 You can add a course by clicking on the button now.
23:38 8/20/2014 Adding and editting course work correctly now.
17:33 10/1/2014 Add delete course button.
-->
<?php 
	include('css/create-course/button-css.php');
	include('js/create-course/delete-course-button-js.php'); 
 	include('functions/postback.php'); 
	include('js/create-course/tutorial.php'); 
?>
<div class="col-md-4">
</div>
<div class="col-md-4">
</div>
<div class="col-md-4">
	<label style="display:hidden;float:right" id="label1" data-placement="top" data-content="Click it again to turn off tutorials." for="tutorial1"></label>
	<button type="button" class="btn btn-success" style="float:right" id="tutorial1">Tutorials</button>
</div>	
<form>
</form>
	<?php
		$user_id = $user['user_id'];
		$q = "SELECT * FROM course WHERE instructor_id = $user_id";
		$r = mysqli_query($dbc, $q);

		while($course_list = mysqli_fetch_assoc($r) ) { ?>
			<div class="col-md-4">
				<form method="post" action="template/create-course/course-info.php">
					<button class="myCCB" name="cc" data-placement="right" data-content="Click to modify the quiz." id="select_course" type="submit"><?php echo $course_list['course_number'].$course_list['course_name'].$course_list['section_number']; ?></button>
					<input type="hidden" name="course_id" readonly="readonly" value="<?php echo $course_list['course_id']; ?>">
					<input type="hidden" name="course_number" readonly="readonly" value="<?php echo $course_list['course_number']; ?>">
					<input type="hidden" name="course_name" readonly="readonly" value="<?php echo $course_list['course_name']; ?>">
					<input type="hidden" name="section_number" readonly="readonly" value="<?php echo $course_list['section_number']; ?>">
					<input type="hidden" name="department" readonly="readonly" value="<?php echo $course_list['department']; ?>">
					<input type="hidden" name="year" readonly="readonly" value="<?php echo $course_list['year']; ?>">
					<input type="hidden" name="course_code" readonly="readonly" value="<?php echo $course_list['course_code']; ?>">
				</form>
				<button id="deleteButton" data-placement="left" data-content="Delete this quiz." name="<?php echo $course_list['course_id']; ?>" class="deleteButton" style="position:absolute; top:10px; right:15px;"><i class="fa fa-times"></i></button>
			</div>
	<?php } ?>  
<form>
	<div class="col-md-4">
		<button class="myCCB" id="add_course" data-placement="bottom" data-content="Click to add a new quiz." name="add_course" type="submit" formmethod="post"><i class="fa fa-plus fa-4x"></i></button> <!-- myCCB: my create course button -->
	</div>

</form>