<?php

//Grade quizzes page


#Start session
session_start();
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}

 include('theme_1/header.php'); ?>
 <?php 
 	if(isset($_POST['submitted']) == 1){
 		$answer_grade = mysqli_real_escape_string($dbc, $_POST['answer_grade']);
 		$answer_id = mysqli_real_escape_string($dbc, $_POST['answer_id']);
 		$grade_a = "UPDATE answers SET answer_grade ='$answer_grade' WHERE answer_id = '$answer_id'";
		$r_grade_a = mysqli_query($dbc, $grade_a);

		if($r_grade_a){
				$message = '<p>Grade Submitted!</p>';
			}
			else {
				$message = '<p>Grade was not added because '.mysqli_error($dbc).'</p>';
				$message = '<p>'.$grade_a.'</p>';
			}

 	}
 	if(isset($message)) {
			echo $message;
		}

 	//Query: Questions and Quizzes to get question numbers and titles
 	$q = "SELECT question.question_id, question.question_number, quiz.quiz_title FROM quiz
 	RIGHT OUTER JOIN question ON question.quiz_id = quiz.quiz_id 	
 	WHERE quiz.quiz_id = 1";
	$r = mysqli_query($dbc, $q);
	//Second Query: Answers, Users, and Questions to get who answered what question
	$qq = "SELECT answers.question_id, answers.answer_id, answers.user_id, answers.answer_text, answers.answer_grade, user.last_name, user.first_name, question.question_text FROM answers
	INNER JOIN user ON answers.user_id = user.user_id
	INNER JOIN question ON answers.question_id = question.question_id
	WHERE question.quiz_id = 1";
	$rr = mysqli_query($dbc, $qq);
	//Make results of first query into an array
	$quiz_info = array();
	while($qx = $r->fetch_assoc()){
		$quiz_info[ $qx['quiz_title'] ][ $qx['question_id'] ] = $qx['question_number'];

	}

	$qt = array_keys($quiz_info);
	$qt = $qt[0];
	//Make results of secondy query into an array
	$answer_info = array();
	while($qy = $rr->fetch_assoc()){
		$answer_info[ $qy['question_id'] ]['question_text'] = $qy['question_text'];
	
		$answer_info[ $qy['question_id'] ][ $qy['answer_id'] ] = [ 'answer_text'=>$qy['answer_text'], 'name'=>$qy['last_name'].", ".$qy['first_name'], 'answer_id'=>$qy['answer_id'], 'answer_grade'=>$qy['answer_grade'] ];
	}

	$qu = array_keys($answer_info);

	?>

<h1>Grade Quiz: <?php echo $qt; ?></h1>


<div class="row">
	<div class="col-md-9 col-md-offset-1">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" id="vq-grade">
			<?php //foreach question in the quiz, make it put out a li with a data-toggle a to make a tab
				foreach ($quiz_info[$qt] as $qid=>$qnum) {
								
			?>
		  <li><a href="#q<?php echo $qid;?>" 
		  	data-toggle="tab">Question <?php echo $qnum;?></a></li>
		   <?php } ?>
		</ul>

		<div class="tab-content">
			<?php

			foreach ($answer_info as $aqid=>$qna_array) {
			?>
		  <div class="tab-pane fade" id="q<?php echo $aqid; ?>">
		  	<h3><?php echo $qna_array['question_text'];?></h3>
		  	
		  	<?php //foreach answer_id in the query, make it spit out who answered it and their answer
		  	foreach ($qna_array as $answer_id => $answer_info) {
		  			if(!is_numeric($answer_id)){
		  				
		  			} else {
		  	?>
			  	<p><?php echo $answer_info['name'];?></p>
			  	<div class="row">			  		
				  	<blockquote class="col-md-7">
				  		<p><?php echo $answer_info['answer_text'];?>
				  		</p>
				  	</blockquote>
				  	<div class="col-md-2"></div>
				  	<div class="col-md-3">
				  		<form role="form" type="post">
				  			<div class="form-group" class="form-inline">
				  			Grade:
				  			<label class="sr-only" for="answer_grade">Response Grade</label>
				  			<input type="hidden" name="answer_id" value="<?php echo $answer_info['answer_id']?>">
    						<input type="text" class="form-control" style="width: 30%; display: inline-block;" name="answer_grade" id="answer_grade" placeholder="<?php echo $answer_info['answer_grade'];?>">
    						<input type="hidden" name="submitted" value="1">
    						<button type="submit" class="btn btn-vquiz" formmethod="post">Save</button>
    						<?php if(empty($answer_info['answer_grade'])){
    							echo "<span class='badge'>Ungraded Answer</span>";
    						}
    							?>

				  			</div>
				  		</form>
				  	</div>
				</div>
				<?php } 
				}	?>
				</div>
				<?php }
				 ?>
		  
		</div>
	</div>
</div>

<script>
	//When a tab is clicked, show the corresponding pane	
	$('#vq-grade a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});
	//add class of "active" to first li of nav-tabs	
	$(".nav-tabs li:first").addClass("active");
	//add class of "in active" to first instance of tab-pane
	$(".tab-pane:first").addClass("in active");

	
   
</script>


<?php if($debug == 1) { include('widgets/debug.php'); } ?>	  

<?php include('theme_1/footer.php'); ?>