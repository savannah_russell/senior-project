<?php
	include('config/connection.php');
	include('functions/postback.php');
	include('css/css.php'); 
	include('css/sign-up-css.php');
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Sign up</title>
	</head>
	<body>
	 	<div id="wrap">
		    <div class="container">
		    	<div class="row">

					<div class="col-md-6 col-md-offset-3">	
						<div class="panel panel-info">
							<div class="panel-heading">
						    	<h1>Sign up</h1>
						  	</div>
						  	<div class="panel-body">  
						    	<form>
						      		<div class="form-group">
								        <label for="first-name">First name</label>
								        <input type="text" class="form-control" id="first-name" name="first_name" placeholder="First name">
						      		</div>

						      		<div class="form-group">
								        <label for="middle-name">Middle name</label>
								        <input type="text" class="form-control" id="middle-name" name="middle_name" placeholder="Middle name">
						      		</div>

						      		<div class="form-group">
								        <label for="last-name">Last name</label>
								        <input type="text" class="form-control" id="last-name" name="last_name" placeholder="Last name">
						      		</div>

						      		<div class="form-group">
								        <label for="email">Email address</label>
								        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
						      		</div>

						      		<div class="form-group">
								        <label for="password">Password</label>
								        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
						      		</div>

						      		<div class="form-group">
						    			<button type="submit" formmethod="post" name="sign_up_confirm" class="btn btn-primary">Sign up</button>
						    		</div>
						    	</form>
						  	</div>
						</div><!-- end panel -->
					</div>
					

					
				</div><!-- end row -->
				

			</div>
		</div>
		<?php include('template/footer.php'); ?>
	</body>
</html>