<html>
	<head>
		<?php
			include('config/setup.php');
			include('template/check-user.php');
			include('functions/postback.php');
			include('css/sign-up-course-css.php');
		?>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" >
		$(document).ready(function(){
			$("#tutorial1").click(function() {
				$('#course_code').popover('toggle');
				$('#label1').popover('toggle');
			});
		});
		</script>
	</head>
	<body>
		<div id="wrap">
			<?php include('template/navigation.php'); ?>

			<div class="container">
				<h1 style="display:inline">Sign Up for Course</h1>				
				<button type="button" class="btn btn-success" style="float:right" id="tutorial1">Tutorials</button>
				<label style="display:hidden;float:right" id="label1" data-placement="left" data-content="Click it again to turn off tutorials." for="tutorial1"></label>
				</br></br>
				
				<div class="col-md-6 col-md-offset-3">	
					<div class="panel panel-info">

					  	<div class="panel-body">  
				    	<form>
				      		<div class="form-group">
						        <label for="course_code">Course code</label>
						        <input type="course_code" data-placement="right" data-content="Get the course code from your professor." class="form-control" id="course_code" name="course_code" placeholder="Course code">
				      		</div>

				      		<div class="form-group">
				    			<button type="submit" formmethod="post" name="sign_up_course" class="btn btn-danger">Sign up</button>
				      		</div>	
				    	</form>
					  	</div>

					</div><!-- end panel -->
				</div>
			</div>
		</div>

		<?php include('template/footer.php'); ?>
	</body>
</html>