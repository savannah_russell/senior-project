
<?php 
// name: create quiz
// author: Paul
// function: In this web page, the professor can create a quiz and add the question.
// 
// modification log: 
// 

#Start session
session_start();
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}
?>
<style>
    label, input { display:block; }
</style>
<?php include('theme_1/header.php'); ?>
	
<h1>Create Quiz</h1>
	
	<!--E1  upload image function -->
	<form enctype="multipart/form-data" method="post">
		Upload Image: <br />
		<input name="upfile" type="file">
		<input type="submit" name="send" value="Upload" class="btn btn-vquiz extra-v-margin">
		<p>Permitted file types for Upload: .jpg, .jpeg, .png, .pjpeg, .gif, .bmp, .x-png, .swf</p> 
	</form>
	<!--E2  upload image function -->
	<form>
		<button type="submit" name="create" formmethod="post" class="btn btn-default" id="create-question-window">Add a question <i class="fa fa-plus"></i></button>
	</form>
<!--Z1  create a question in accordion -->
<div class="panel-group" id="accordion" >
    <?php
		$q = "SELECT * FROM question";
		$r = mysqli_query($dbc, $q);

		while($question_list = mysqli_fetch_assoc($r)) { ?>  
			<div class="panel panel-default"> <!-- class="panel||accordion ..." see what change-->
    			<div class="panel-heading">
	      			<h4 class="panel-title">
		        		<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $question_list['question_id'] ?>">
		          			Question <?php echo $question_list['question_id'] ?>
		        		</a>
	      			</h4>
    			</div>
			    <div name="<?php echo $question_list['question_id'] ?>" id="<?php echo $question_list['question_id'] ?>" class="panel-collapse collapse"> <!-- style="display: none;" -->
			    	<div class="panel-body">
			    		<form>
				    	<!--	<label for="name">Question_id</label>  -->
						    <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $question_list['question_id'] ?>">
				        	<label for="name">Question Number</label>
						    <input type="text" name="question_number" value="<?php echo select_my_data($dbc, 'question', 'question_number', $question_list['question_id']);?>"> 
				        	<label for="name">Question</label>
						    <input type="text" name="question" value="<?php echo select_my_data($dbc, 'question', 'question_text', $question_list['question_id']);?>"> 
						    <label for="email">Answer</label>
						    <input type="text" name="answer" value="<?php echo select_my_data($dbc, 'question', 'answer_key', $question_list['question_id']);?>" >
						    <label for="password">Image</label>

						  	<select class="form-control" name="image_list" id="select">								
								<?php 
									$q1 = "SELECT * FROM image";
									$r1 = mysqli_query($dbc, $q1);

									while ($Image_list = mysqli_fetch_assoc($r1)) {
										$Image_list = data_image_list($dbc, $Image_list['image']);
									?>
									<option value="<?php echo $Image_list['image_list']?>"><?php echo $Image_list['image_list']?></option>

								<?php } ?>
							</select>
							<button type="submit" name="save" formmethod="post">save</button>
						</form>
			      	</div>
			    </div>
			</div>		
	<?php } ?>
</div>


<?php if($debug == 1) { include('widgets/debug.php'); } ?>

<?php include('theme_1/footer.php'); ?>

