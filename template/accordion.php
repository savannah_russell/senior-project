<!-- 
name: accordion
author: Paul & Savannah
function: Set up the accordion.
 
modification log: 
-->

<?php 
	include('css/accordion-css.php'); 
	include('functions/postback.php');
?>


<div class="panel-group" id="accordion" >
    <?php
		$q = "SELECT * FROM question";
		$r = mysqli_query($dbc, $q);

		while($question_list = mysqli_fetch_assoc($r)) { ?>  
			<div class="panel panel-default"> <!-- class="panel||accordion ..." see what change-->
    			<div class="panel-heading">
	      			<h4 class="panel-title">
		        		<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $question_list['question_id'] ?>">
		          			Question <?php echo $question_list['question_number'] ?>
		        		</a>
	      			</h4>
    			</div>
			    <div name="<?php echo $question_list['question_id'] ?>" id="<?php echo $question_list['question_id'] ?>" class="panel-collapse collapse"> <!-- style="display: none;" -->
			    	<div class="panel-body">
			    		<form>
				    	<!--	<label for="name">Question_id</label>  -->
						    <input type="hidden" name="questionID" readonly="readonly" value="<?php echo $question_list['question_id']; ?>">
				        	
				        	<label for="question_number">Question Number</label>
						    <input type="text" id="question_number" name="question_number" value="<?php echo $question_list['question_number']; ?>"> <!-- select_my_data($dbc, 'question', 'question_number', $question_list['question_id']) -->
				        	
				        	<label for="image_list">Image</label>
				        	<select class="form-control" id="image_list" name="image_list">								
								<?php 
									$q1 = "SELECT * FROM image";
									$r1 = mysqli_query($dbc, $q1);

									while ($Image_list = mysqli_fetch_assoc($r1)) {
										//$Image_list = select_my_list($dbc, 'image', 'image', $Image_list['image']);
									?>
									<option value="<?php echo $Image_list['image']?>"><?php echo $Image_list['image']?></option>

								<?php } ?>
							</select></br>
				        	
				        	<label for="question">Question</label></br>
						    <textarea id="question" name="question"><?php echo $question_list['question'];?></textarea>
						    
						    <label for="answer">Answer</label></br>
						    <textarea type="text" id="answer" name="answer"><?php echo $question_list['answer'];?></textarea>
		  	
							<button type="submit" name="save_question" formmethod="post">save</button>
						</form>
			      	</div>
			    </div>
			</div>		
	<?php } ?>
</div>