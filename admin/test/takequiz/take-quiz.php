<!DOCTYPE html>
<html>
	<head>
		<?php include('js.php'); ?>
		<?php include('css.php'); ?>
		<?php include('../popup-window-fileUpload.php'); ?>
		<style>
			#test{
				margin-left: auto;
    		margin-right: auto;
			}
			#texttest{
				color: black;
			}
		</style>
	</head>
	<body>

      
     <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <?php 
          $q = "SELECT * FROM question";
          $r = mysqli_query($dbc, $q);

          while ($Image_list = mysqli_fetch_assoc($r)) {
            $Image_list = data_question_list($dbc, $Image_list['question']);
          ?>
          <li data-target="#myCarousel" ></li>

        <?php } ?>

      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="../upload/panda.jpg" alt="First slide" id="test">
          <div class="container">
            <div class="carousel-caption">
              <h1>Example headline.</h1>
              <textarea rows="4" cols="50" id="texttest">
                <?php 
                  $q1 = "SELECT * FROM image";
                  $r1 = mysqli_query($dbc, $q1);

                  while ($Image_list = mysqli_fetch_assoc($r1)) {
                    $Image_list = data_image_list($dbc, $Image_list['image']);
                  ?>
                  <?php echo $Image_list['image_list']?>

                <?php } ?>
              </textarea> 
            </div>
          </div>
        </div>



        <div class="item">
          <img src="../upload/panda.jpg" alt="Second slide" id="test">
          <div class="container">
            <div class="carousel-caption">
              <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              
            </div>
          </div>
        </div>
        <div class="item">
          <img src="../upload/panda.jpg" alt="Third slide" id="test">
          <div class="container">
            <div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
             
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
	</body>
</html>