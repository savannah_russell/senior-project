<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		$("#edit").click(function() {
		    var $this = $(this);
		    $this.css("display","none");
		    $("#save").css("display","inline");
		    $("#course-number").attr("readonly", false);
		    $("#course-name").attr("readonly", false);
		    $("#section-number").attr("readonly", false);
		    $("#department").attr("readonly", false);
		    $("#year").attr("readonly", false);
		});
		$("#save").click(function() {
		    var $this = $(this);
		    $this.css("display","none");
		    $("#edit").css("display","inline");
		});

	});

</script>