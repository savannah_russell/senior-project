<!-- 
name: check user
author: Paul & Savannah
function: If the user hasn't login, it goes back to the login page.
 
modification log:  
-->

<?php
if(!isset($_SESSION['username'])) {
	header('Location: login.php');
}
?>