<?php
	include('../../config/connection.php');
	include('../../functions/postback.php');
	include('../../css/css.php'); 
	include('../../css/sign-up-css.php');
	include('../../js/create-course/tutorial.php');
	include('../../js/create-course/edit-course-info.php');
?>
</br></br>
<div class="col-md-6 col-md-offset-3">	
	<div class="panel panel-info">
		<div id="title" data-placement="left" data-content="Don't refresh this page." class="panel-heading">
	    	<h1>Course information</h1>
	    	
	  	</div>

	  	<div class="panel-body"> 
	  		<label style="display:hidden;float:right" id="label2" data-placement="top" data-content="Click it again to turn off tutorials." for="tutorial2"></label>
	  		<button type="button" class="btn btn-danger" style="float:right" id="tutorial2">Tutorials</button>
	    	<form>
	    		<div class="form-group">
			        <input type="hidden" name="course_id" readonly="readonly" value="<?php echo $_POST['course_id']; ?>">
	      		</div>
	      		<div class="form-group">
			        <label for="course-number">Course number</label>
			        <input type="text" class="form-control" id="course-number" name="course_number" value="<?php echo $_POST['course_number']; ?>" readonly>
	      		</div>

	      		<div class="form-group">
			        <label for="course-name">Course name</label>
			        <input type="text" class="form-control" id="course-name" name="course_name" value="<?php echo $_POST['course_name']; ?>" readonly>
	      		</div>

	      		<div class="form-group">
			        <label for="course-name">Section number</label>
			        <input type="text" class="form-control" id="section-number" name="section_number" value="<?php echo $_POST['section_number']; ?>" readonly>
	      		</div>

	      		<div class="form-group">
			        <label for="department">Department</label>
			        <input type="text" class="form-control" id="department" name="department" value="<?php echo $_POST['department']; ?>" readonly>
	      		</div>

	      		<div class="form-group">
	      			<label for="Year">Year</label>
			        <input type="text" class="form-control" id="year" name="year" value="<?php echo $_POST['year']; ?>" readonly>
	      		</div>

	      		<div class="form-group">
	      			<label for="Year">Course code</label>
			        <input type="text" class="form-control" id="course-code" data-placement="top" data-content="This code will be generated automatically. Give this code to your students so that they can enroll in this course." name="course_code" value="<?php echo $_POST['course_code']; ?>" readonly>
	      		</div>


	      		<div class="form-group">
	    			<button type="button" id="edit" class="btn btn-primary" style="display:inline">Edit</button>
	    			<button type="submit" id="save" formmethod="post" name="save_course" class="btn btn-primary" style="display:none">Save</button>
	    			<button type="submit" formmethod="post" name="go_back" class="btn btn-primary">Go back</button>
	    		</div>
	    	</form>
	  	</div>
	</div><!-- end panel -->
</div>