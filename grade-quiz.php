<!DOCTYPE html>

<html>
	<head>
		<?php
			include('config/setup.php');
			include('template/check-user.php');
		?>

	</head>
	<body>
		<div id="wrap">
			<?php include('template/navigation.php'); ?>

			<div class="container">

				<?php include('template/grade-quiz/carousel.php'); ?>

			</div>
		</div>

		<?php include('template/footer.php'); ?>
	</body>
</html>