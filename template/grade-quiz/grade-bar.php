<!-- 
name: grade bar
author: Paul 
function: Set up the grade bar.
 
modification log:

-->
<?php include('css/grade-quiz/grade-bar-css.php'); ?>

<div class="row">
	<form>
    	<div class="col-md-8">

		</div>

		<div class="col-md-2">
			<select id="grade-bar" name="point">
    			<?php
    				$i = 0;
					while($i <= 20) { ?>
						<option value="point"><?php echo $i; $i++; ?></option>	
				<?php }?>	
			</select>
		</div>

    	<div class="col-md-2">
			<button type="submit" id="save-button" class="btn btn-warning" name="save_grade" style="color:black" formmethod="post">save</button>
    	</div>


	</form>
</div>