<?php
// CSS File;



?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

<style>
	html,body {
		height: 100%;
	}
	#wrap {
		min-height: 100%;
		height: auto;
		margin: 0 auto -60px;
		padding: 0 0 60px;
	}
	#footer{
		height: 60px;
		background-color: #f5f5f5;
	}

	#btn-debug {
		position: absolute;
	}

	#console-debug {
		position: absolute;
		top: 50px;
		left: 0px;
		width: 30%;
		height: 700px;
		overflow-y: scroll; 
		background-color: #FFFFFF;
		box-shadow: 2px 2px 5px #CCCCCC;
	}

	#console-debug pre {
		
	}

	#wrap {
		width: 1000px;
		margin-left: auto;
		margin-right: auto;
	}
	.container {
		width: 100%;
	}
	.user-buttons {
		float: right;
		margin-right: 20px;
		margin-top: 7px;
	}
	.btn-vquiz {
		background-color: #1abc9c;
		color: #ffffff;
	}
	.btn-vquiz:hover {
		background-color: #16a085;
		color: #ffffff;
	}
	.bargraph {
		width: 375px;
		height: 275px;
		margin: 10px;
		margin-left: auto;
		margin-right: auto;
		position: relative;
		border: 1px solid #d5d5d5;

	}
	.bargraph ul.bars {
		margin: 0;
		padding: 0;
		list-style-type: none;
	}
	.bargraph ul.bars li{
		position: absolute;
		text-align: center;
		color: #ffffff;
		font-size: 14px;
		width: 50px;
		bottom: 22px;
		height: 200px;
	}
	.bargraph ul.bars li.bar1{
		left: 21px;
		background-color: #c0392b;
		height: 140px; 
	}
	.bargraph ul.bars li.bar2{
		left: 86px;
		background-color: #f1c40f;
		height: 92px;
	}
	.bargraph ul.bars li.bar3{
		left: 151px;
		background-color: #3498db;
		height: 40px;
	}
	.bargraph ul.bars li.bar4{
		left: 212px;
		background-color: #8e44ad;
		height: 200px;
	}
	.graphlabel {
		list-style-type: none;
		position: absolute;
		bottom: 0px;
		margin: 0;
		padding: 0;
		background-color: #7f8c8d;
		width: 100%;

	}
	.graphlabel li {
		color: #ffffff;
		text-align: center;
	}
	.ylabel {
		list-style-type: none;
		position: absolute;
		left: -65px;
		bottom: 0;
		width: 40px;
		text-align: right;
		margin: 0;
		padding: 0;

	}
	.ylabel li {
		color: #7f8c8d;
		height: 51px;
		line-height: 51px;
		text-align: right;
	}
	.ylabel li:first-child {
		height: 35px;
		line-height: 35px;
	}
	.ylabel li.units {
		position: absolute;
		bottom: 110px;
		right: 20px;
		line-height: 1.2em;
		height: auto;
	}
	.panel-info>.panel-heading{
		color: #ffffff;
		background-color: #1abc9c;
		background-image: none;
	}
	.navbar-default{
		color: #ffffff;
		background-image: none;
		background-color: #1abc9c;
	}
	.navbar-default .navbar-nav>li>a{
		color: #ffffff;
	}
	.navbar-default .navbar-nav>li>a:hover{
		color: #ffffff;
	}
	.carousel-control.left, .carousel-control.right{
		background:none;
	}
	.carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-prev, .carousel-control .icon-next{
		color: #2d6ca2;
	}


</style>