<?php	
	include('css/create-quiz/button-css.php'); 
	include('functions/take-quiz/postback.php');
	include('js/take-quiz/tutorial.php');
?>
<form>
	<div class="col-md-2">
		<select class="form-control" name="course_list">												
			<?php 
				$user_id = $user['user_id'];
				$user_status = $user['status'];

				if($user_status == 'professor') {
					$q = "SELECT * FROM course WHERE instructor_id = $user_id";
				} elseif ($user_status == 'student') {
					$q = "SELECT * FROM course RIGHT JOIN user_course on course.course_id = user_course.course_id WHERE user_id = $user_id";
				}
				
				$r = mysqli_query($dbc, $q);
				
				$q1 = "SELECT td3 FROM user WHERE user_id = '$user_id'";
				$r1 = mysqli_query($dbc, $q1);
				$course_id = mysqli_fetch_row($r1);
				while ($Course_list = mysqli_fetch_assoc($r)) {
					//$Image_list = select_my_list($dbc, 'image', 'image', $Image_list['image']);
				?>
				<option value="<?php echo $Course_list['course_id']; ?>" <?php if($Course_list['course_id'] == $course_id[0]) echo "selected=\"selected\""; ?>><?php echo $Course_list['course_name']." ".$Course_list['section_number']; ?></option>

			<?php } ?>
		</select>
	</div>
	<button type="submit" id="go" name="select_course" formmethod="post" data-placement="right" data-content="You need to select a course first!" class="btn btn-danger">go</button>
	<button type="button" class="btn btn-success" style="float:right" id="tutorial1">Tutorials</button>
	<label style="display:hidden;float:right" id="label1" data-placement="left" data-content="Click it again to turn off tutorials." for="tutorial1"></label>
</form>

<?php
	$user_id = $user['user_id'];
	$q1 = "SELECT * FROM quiz WHERE course_id = (SELECT td3 FROM user WHERE user_id = $user_id)";
	$r1 = mysqli_query($dbc, $q1);

	while($Quiz_list = mysqli_fetch_assoc($r1) ) { ?> 
		<div class="col-md-4">
			<form>	
				<input type="hidden" name="quiz_id" readonly="readonly" value="<?php echo $Quiz_list['quiz_id']; ?>">

	<!-- 			<button id="mySCB" type="submit" onclick="selectQuiz()"></button> -->
				<input type="submit" data-placement="right" data-content="Click to get into the quiz. Grey means this quiz is no long available." formmethod="post" name="select_quiz" value="<?php echo $Quiz_list['quiz_name']." ".$Quiz_list['quiz_date']; ?>" id="select_quiz" class="mySCB" <?php if($Quiz_list['availability']==0) echo "style=\"background:grey\""." disabled"; ?>>
			</form>	
		</div>
<?php } ?> 